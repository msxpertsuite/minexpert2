/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2019 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */

/////////////////////// StdLib includes
#include <iostream>
#include <cstdio>
#include <iomanip>
#include <thread>
#include <ctime>

/////////////////////// OpenMP include
#include <omp.h>

/////////////////////// Qt includes
#include <QDebug>
#include <QDir>
#include <QMessageBox>
#include <QFileDialog>
#include <QMenuBar>
#include <QMenu>
#include <QThread>

/////////////////////// pappsomspp includes
#include <pappsomspp/msfile/msfileaccessor.h>

/////////////////////// Local includes
#include "MsRunLoader.hpp"
#include "MsRunProbeTask.hpp"
#include "../nongui/MsRunDataSet.hpp"


int MsRunLoaderMetaTypeId = qRegisterMetaType<MsXpS::minexpert::MsRunLoader>(
  "MsXpS::minexpert::MsRunLoader");

int MsRunLoaderSPtrMetaTypeId =
  qRegisterMetaType<MsXpS::minexpert::MsRunLoaderSPtr>(
    "MsXpS::minexpert::MsRunLoaderSPtr");


namespace MsXpS
{

namespace minexpert
{

MsRunLoader::MsRunLoader(ProgramWindow *program_window_p,
                         pappso::MsFileAccessorSPtr ms_file_accessor_sp)
  : QObject(),
    mp_programWindow(program_window_p),
    msp_msFileAccessor(ms_file_accessor_sp)
{
  if(program_window_p == nullptr)
    qFatal("Cannot be that the pointer is nullptr.");
  if(msp_msFileAccessor == nullptr || msp_msFileAccessor.get() == nullptr)
    qFatal("Cannot be that the pointer is nullptr.");
}


MsRunLoader::~MsRunLoader()
{
}

} // namespace minexpert

} // namespace MsXpS
