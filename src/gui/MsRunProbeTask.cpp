/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2019 Filippo Rusconi
 *
 * Inspiration from XTPcpp by Olivier Langella (olivier.langella@u-psud.fr)
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


/////////////////////// StdLib includes
#include <iostream>
#include <chrono>

/////////////////////// Qt includes
#include <QDebug>
#include <QSettings>
#include <QThread>
#include <QMessageBox>


/////////////////////// pappsomspp includes
#include <pappsomspp/msfile/msfileaccessor.h>
#include <pappsomspp/msrun/msrunreadconfig.h>
#include <pappsomspp/exception/exceptioninterrupted.h>


/////////////////////// Local includes
#include "MsRunProbeTask.hpp"
#include "../nongui/MassSpecDataFileLoader.hpp"


int MsRunProbeTaskMetaTypeId =
  qRegisterMetaType<MsXpS::minexpert::MsRunProbeTask>(
    "MsXpS::minexpert::MsRunProbeTask");

int MsRunProbeTaskSPtrMetaTypeId =
  qRegisterMetaType<MsXpS::minexpert::MsRunProbeTaskSPtr>(
    "MsXpS::minexpert::MsRunProbeTaskSPtr");


namespace MsXpS
{
namespace minexpert
{


MsRunProbeTask::MsRunProbeTask(ProgramWindow *program_window_p)
  : mp_programWindow(program_window_p)
{
}


MsRunProbeTask::~MsRunProbeTask()
{
  // qDebug() << "MsRunProbeTask::MsRunProbeTask destructor";
}


void
MsRunProbeTask::probeMsRun(MsRunProbeTask *ms_run_probe_task_p,
                           MsRunProberSPtr ms_run_prober_sp)
{
  // We need to ensure that we react to a signal that indeed is directed to the
  // proper MsRunProbeTask, because the signal emanates from a generic emitter:
  // ProgramWindow.

  if(this != ms_run_probe_task_p)
    return;

  MsRunProbeInfoSPtr ms_run_probe_info_sp = nullptr;

  const auto start_time = std::chrono::steady_clock::now();

  try
    {
      ms_run_probe_info_sp =
        std::make_shared<MsRunProbeInfo>(ms_run_prober_sp->probe());
    }
  catch(pappso::ExceptionInterrupted &e)
    {
      qDebug() << "Interruption:" << e.what();

      // Note how ms_run_probe_info_sp could not be returned
      // by the probe() function, thus it is nullptr. It is
      // the responsibility of the downstream code to check
      // if the ms_run_probe_info_sp pointer is nullptr, or, if
      // not, if ms_run_probe_info_sp->probeErrors is not empty.

      emit finishedProbingMsRunSignal(
        ms_run_prober_sp, ms_run_probe_info_sp, 0);

      return;
    }

  const auto end_time = std::chrono::steady_clock::now();

  double duration_in_seconds =
    std::chrono::duration_cast<std::chrono::seconds>(end_time - start_time)
      .count();

  qDebug("Finished probing MS run. Time used (seconds): %f",
         duration_in_seconds);

  // At this point, emit the signal that we did the job.
  emit finishedProbingMsRunSignal(
    ms_run_prober_sp, ms_run_probe_info_sp, duration_in_seconds);
}


} // namespace minexpert

} // namespace MsXpS
