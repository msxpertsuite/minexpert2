/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2019 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */

#pragma once


/////////////////////// StdLib includes


/////////////////////// Qt includes
#include <QMainWindow>


/////////////////////// pappsomspp includes
#include <pappsomspp/widget/plotwidget/ticxicchromtraceplotwidget.h>


/////////////////////// Local includes
#include "ui_MsRunProbeInfoViewerDlg.h"
#include "BasePlotCompositeWidget.hpp"
#include "../nongui/MassDataIntegrator.hpp"
#include "../nongui/MsRunProber.hpp"

namespace MsXpS
{
namespace minexpert
{


class MsRunDataSet;
class ProgramWindow;
class DataPlottableNode;

class MsRunProbeInfoViewerDlg : public QDialog
{
  Q_OBJECT

  public:
  // Construction/destruction
  MsRunProbeInfoViewerDlg(QWidget *parent,
                          const QString &title,
                          const QString &settingsTitle,
                          const QString &description,
                          const QString &vendor,
                          pappso::MsRunIdCstSPtr ms_run_id_csp,
                          MsRunProbeInfoSPtr ms_run_probe_info_sp);
  virtual ~MsRunProbeInfoViewerDlg();

  virtual void writeSettings();
  virtual void readSettings();

  virtual bool initialize();
  virtual void closeEvent(QCloseEvent *event) override;

  ProgramWindow *getProgramWindow();

  virtual void show();

  void showWindow();


  public slots:

  signals:

  protected:
  pappso::TicXicChromTracePlotWidget *mp_plotWidget;

  bool m_areRtTimeUnitSeconds = true;

  Ui::MsRunProbeInfoViewerDlg m_ui;

  ProgramWindow *mp_programWindow = nullptr;

  QString m_title;
  QString m_settingsTitle;
  QString m_description = "MS run data probe for vendor ";
  QString m_vendor;

  pappso::MsRunIdCstSPtr m_msRunIdCstSPtr;

  MsRunProbeInfoSPtr msp_msRunProbeInfo;

  void convertTicChromatogramRetTimeUnit();

  void updateRtRangeSpinBoxes();

  bool validateData();
};


} // namespace minexpert

} // namespace MsXpS
