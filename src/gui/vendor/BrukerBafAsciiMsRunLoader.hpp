/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2019 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#pragma once


/////////////////////// StdLib includes


/////////////////////// Qt includes
#include <QMainWindow>
#include <QDir>


/////////////////////// pappsomspp includes
#include <pappsomspp/msfile/msfileaccessor.h>


/////////////////////// Local includes
#include "config.h"

#include "../MsRunLoader.hpp"
#include "../../nongui/MsRunProber.hpp"
#include "../MsRunProbeTask.hpp"
#include "../MsRunReadTask.hpp"
#include "../ProgramWindow.hpp"
#include "../ConsoleWnd.hpp"
#include "../../nongui/MassSpecDataFileLoader.hpp"

namespace MsXpS
{
namespace minexpert
{

class BrukerBafAsciiMsRunLoader : public MsRunLoader
{
  Q_OBJECT

  public:
  explicit BrukerBafAsciiMsRunLoader(
    ProgramWindow *program_window_p,
    pappso::MsFileAccessorSPtr ms_file_accessor_sp);
  virtual ~BrukerBafAsciiMsRunLoader();

  void probeMsRun(bool full_in_memory = false);
  void readMsRun(pappso::MsRunReadConfig ms_run_reader_config,
                 bool full_in_memory = false);
  virtual void loadMsRun(bool full_in_memory = false) override;

  public slots:

  void finishedReadingMsRunData(MsRunDataSetSPtr ms_run_data_set_sp,
                                double duration_seconds);

  signals:

  void readMsRunDataSignal(MsRunReadTask *ms_run_read_task_p,
                           pappso::MsRunReaderSPtr ms_run_reader_sp,
                           const pappso::MsRunReadConfig &ms_run_read_config,
                           std::shared_ptr<MassSpecDataFileLoader> loader_sp);

  protected:
};

typedef std::shared_ptr<BrukerBafAsciiMsRunLoader> BrukerBafAsciiMsRunLoaderSPtr;

} // namespace minexpert

} // namespace MsXpS


Q_DECLARE_METATYPE(MsXpS::minexpert::BrukerBafAsciiMsRunLoader)
extern int BrukerBafAsciiMsRunLoaderMetaTypeId;

Q_DECLARE_METATYPE(MsXpS::minexpert::BrukerBafAsciiMsRunLoaderSPtr)
extern int BrukerBafAsciiMsRunLoaderSPtrMetaTypeId;
