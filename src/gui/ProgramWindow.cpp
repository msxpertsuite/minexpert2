/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2019 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */

/////////////////////// StdLib includes
#include <libXpertMassGui/IsotopicClusterGeneratorDlg.hpp>
#include <libXpertMassGui/IsotopicClusterShaperDlg.hpp>
#include <libXpertMass/MassDataCborBaseHandler.hpp>
#include <iostream>
#include <cstdio>
#include <iomanip>
#include <thread>
#include <ctime>

/////////////////////// OpenMP include
#include <omp.h>

/////////////////////// Qt includes
#include <QDebug>
#include <QDir>
#include <QMessageBox>
#include <QFileDialog>
#include <QMenuBar>
#include <QMenu>
#include <QKeySequence>
#include <QSettings>
#include <QCloseEvent>
#include <QThread>
#include <QDateTime>
#include <QTimer>

/////////////////////// pappsomspp includes
#include <pappsomspp/msfile/msfileaccessor.h>

/////////////////////// Local includes
#include <libXpertMass/MassDataClient.hpp>
#include <libXpertMass/MassDataServer.hpp>

#include "Application.hpp"
#include "ProgramWindow.hpp"
#include "MsRunReadTask.hpp"
#include "MsRunProbeTask.hpp"
#include "../nongui/MassDataIntegrator.hpp"
#include "MsRunSelectorDlg.hpp"
#include "../nongui/MassDataIntegratorTask.hpp"
#include "../nongui/MsRunDataSet.hpp"
#include "TicXicChromMassSpecColorMapWnd.hpp"
#include "MsRunProbeInfoViewerDlg.hpp"
#include "../nongui/vendor/BrukerTimsTofMsRunProber.hpp"
#include "vendor/BrukerTimsTofMsRunLoader.hpp"
#include "vendor/BrukerBafAsciiMsRunLoader.hpp"
#include "XyMsRunLoader.hpp"

namespace MsXpS
{

namespace minexpert
{

ProgramWindow::ProgramWindow(QWidget *parent, const QString &module_name)
  : QMainWindow(parent), m_applicationName(module_name)
{
  dynamic_cast<QWidget *>(this)->setWindowTitle(m_applicationName);

  m_lastUsedDirectory = QDir::home().absolutePath();

  setupWindow();
}


ProgramWindow::~ProgramWindow()
{
  delete mpa_analysisFile;
  delete mpa_analysisPreferences;

  writeSettings();
}


const QString &
ProgramWindow::moduleName() const
{
  return m_applicationName;
}

void
ProgramWindow::setMaxThreadUseCount(std::size_t count)
{
  m_maxThreadUseCount = count;
}


std::size_t
ProgramWindow::getMaxThreadUseCount()
{
  return m_maxThreadUseCount;
}


void
ProgramWindow::incrementLoadedFileCount()
{
  ++m_loadedFileCount;
}


bool
ProgramWindow::openMassSpectrometryFileDlg(const QString &dir_name,
                                           bool full_in_memory)
{
  // qDebug() << "The directory name is:" << dir_name;

  QFileDialog file_dialog(dynamic_cast<QWidget *>(this),
                          QString("%1 - Select mass spectrometry data file(s)")
                            .arg(m_applicationName));

  file_dialog.setFileMode(QFileDialog::ExistingFiles);
  file_dialog.setViewMode(QFileDialog::Detail);
  file_dialog.setAcceptMode(QFileDialog::AcceptOpen);

  // Open the dialog in the right directory.
  if(dir_name.isEmpty())
    file_dialog.setDirectory(m_lastUsedDirectory);
  else
    file_dialog.setDirectory(dir_name);

  QStringList file_names;

  if(file_dialog.exec())
    {
      file_names = file_dialog.selectedFiles();

      // Store the directory name whence the selected files come.

      if(file_names.size())
        {
          QString last_used_directory;
          QString first_file_name = file_names.at(0);
          QFileInfo file_info     = QFileInfo(first_file_name);
          m_lastUsedDirectory     = file_info.absoluteDir().dirName();
        }
    }

  for(int iter = 0; iter < file_names.size(); ++iter)
    {
      QString cur_file_name = file_names.at(iter);

      openMassSpectrometryFile(cur_file_name, full_in_memory);
    }

  return true;
}


void
ProgramWindow::openMassSpectrometryFile(const QString &file_name,
                                        bool full_in_memory,
                                        const QString &sample_name)
{
  qDebug() << "Opening mass spectrometry file:" << file_name
           << "with sample name:" << sample_name
           << "from thread:" << QThread::currentThread();

  if(file_name.isEmpty() || !QFileInfo::exists(file_name))
    {
      openMassSpectrometryFileDlg(QDir::homePath(), full_in_memory);
      return;
    }

  // At this point, we have a file name. Let's store the directory where the
  // file sits so that later the user can be directed there immediately.

  m_lastUsedDirectory = QFileInfo(file_name).dir().absolutePath();

  // Now let's start the file type identification work.

  // The MsFileAccessor will allow to get a list of MS run ids that can then
  // be used to request a MS run reader appropriate for the MS data file
  // format. Because there might be more than one ms run in a single mzML
  // file, we need to have a common prefix for all those ms run ids.
  pappso::MsFileAccessorSPtr ms_file_accessor_sp =
    std::make_shared<pappso::MsFileAccessor>(file_name, m_msRunIdPrefix);

  // qDebug() << "Could get MsFileAccessor.";

  // It is only when we ask for MS run ids that the file accessor actually
  // detects the format of the file. So start with this step.

  std::vector<pappso::MsRunIdCstSPtr> ms_run_ids =
    ms_file_accessor_sp->getMsRunIds();

  std::size_t run_count = ms_run_ids.size();

  // qDebug() << "The number of ms run ids:" << run_count;

  if(!run_count)
    {
      QMessageBox::information(
        dynamic_cast<QWidget *>(this),
        "Open mass spectrometry file",
        "This file does not contain any recognizable MS run.");
      return;
    }

  int run_index = 0;

  // qDebug() << "The number of ms runs in the file is:" << run_count;

  // If there are more than one run, let the user select the one they are
  // interested in.

  if(run_count > 1)
    {

      // If the user cancels the dialog, returns -1, otherwise returns the
      // index of the ms run id.

      run_index = selectMsRun(ms_run_ids);

      // qDebug() << "The run_index: " << run_index;

      if(run_index == -1)
        return;
    }

  // Get the run id of the vector at the index selected by the user.
  pappso::MsRunIdCstSPtr ms_run_id_csp = ms_run_ids.at(run_index);

  // Store in the MsFileAccessor the pointer to the selected MsRunId. This way,
  // we'll be able later to access the proper MsRunReader.
  ms_file_accessor_sp->setSelectedMsRunId(ms_run_id_csp);

  qDebug() << "The file format was detected as:"
           << pappso::Utils::msDataFormatAsString(
                ms_file_accessor_sp->getFileFormat());

  if(ms_file_accessor_sp->getFileFormat() == pappso::MsDataFormat::brukerTims)
    {
      //  Allocate a specific loader
      BrukerTimsTofMsRunLoader *ms_run_loader_p =
        new BrukerTimsTofMsRunLoader(this, ms_file_accessor_sp);

      ms_run_loader_p->loadMsRun(full_in_memory);

      //  Note that the loader is deleteLater() after its work.
    }
  // End of
  // if(ms_file_accessor.getFileFormat() == pappso::MsDataFormat::brukerTims)
  else if(ms_file_accessor_sp->getFileFormat() == pappso::MsDataFormat::mzML ||
          ms_file_accessor_sp->getFileFormat() == pappso::MsDataFormat::mzXML ||
          ms_file_accessor_sp->getFileFormat() == pappso::MsDataFormat::MGF)
    {
      PwizLiteMsRunLoader *ms_run_loader_p =
        new PwizLiteMsRunLoader(this, ms_file_accessor_sp);
      ms_run_loader_p->loadMsRun(full_in_memory);

      //  Note that the loader is deleteLater() after its work.
    }
  else if(ms_file_accessor_sp->getFileFormat() ==
          pappso::MsDataFormat::brukerBafAscii)
    {
      qDebug()
        << "Handling MS run file reading with MsDataFormat::brukerBafAscii";

      // The data are a file obtained by conversion of Bruker Baf data to ascii
      // by the Bruker DataAnalysis/Compass software. This file format is
      // extremely resource intensive and loading data in streamed mode is not
      // supporte.
      if(!full_in_memory)
        {
          QMessageBox msgBox;
          msgBox.setText(
            "Opening this kind of data file in streamed moded is not yet "
            "supported.");

          msgBox.exec();

          return;
        }

      BrukerBafAsciiMsRunLoader *ms_run_loader_p =
        new BrukerBafAsciiMsRunLoader(this, ms_file_accessor_sp);
      ms_run_loader_p->loadMsRun(full_in_memory);

      //  Note that the loader is deleteLater() after its work.
    }
  else if(ms_file_accessor_sp->getFileFormat() == pappso::MsDataFormat::xy)
    {
      qDebug() << "Handling MS run file reading with MsDataFormat::xy";

      XyMsRunLoader *ms_run_loader_p =
        new XyMsRunLoader(this, ms_file_accessor_sp);
      ms_run_loader_p->loadMsRun(full_in_memory);

      //  Note that the loader is deleteLater() after its work.
    }

  return;
}


void
ProgramWindow::openMassSpectrometryFileFromClipBoard()
{
  QClipboard *clipboard = QGuiApplication::clipboard();
  QString ms_data       = clipboard->text();

  QDateTime current_date_time = QDateTime::currentDateTime();

  QString time_as_string = current_date_time.toString("yyyyMMdd-HHmmss");

  openMassSpectrometryFileFromText(ms_data, "clipBoard-" + time_as_string);
  return;
}


void
ProgramWindow::openMassSpectrometryFileFromText(const QString &text,
                                                const QString &sample_name)
{

  // qDebug() << "Opening mass spectrum from text with sample name:"
  //<< sample_name;

  if(text.isEmpty())
    {
      dynamic_cast<QMainWindow *>(this)->statusBar()->showMessage(
        QString("The clipboard is empty"), 4000);

      return;
    }

  // Now write the text to a temporary file and load it.

  // Get a directory suitable for temp files.

  QDir sys_tmp_dir = QDir::temp();

  // Craft the sample name with the date and time

  QString temp_file_name = m_applicationName + "-";

  temp_file_name.append(
    QDateTime::currentDateTime().toString("ddMMyyyy-HHmmss"));

  QFile file(sys_tmp_dir.absolutePath() + '/' + temp_file_name);

  // qDebug() << "Opening file: " << file.fileName();

  if(!file.open(QIODevice::WriteOnly | QIODevice::Text))
    qFatal("Failed to open destination file.");

  QTextStream out(&file);

  out << text;

  out.flush();

  file.close();

  // qDebug() << "File " << file.fileName() << "has been closed now.";

  openMassSpectrometryFile(file.fileName(), true, sample_name);

  return;
}


void
ProgramWindow::openAnalysisPreferencesDlg()
{
  if(mp_analysisPreferencesDlg == nullptr)
    {
      mp_analysisPreferencesDlg = new AnalysisPreferencesDlg(
        dynamic_cast<QWidget *>(this), m_applicationName);
    }

  int res = dynamic_cast<QDialog *>(mp_analysisPreferencesDlg)->exec();

  if(res == QDialog::Accepted)
    {
      // Copy the preferences from the dialog window to *this.
      mpa_analysisPreferences = new AnalysisPreferences(
        mp_analysisPreferencesDlg->analysisPreferences());

      // Let's check if the recording should go a file, in which case we test
      // that we can open it properly.

      if((mpa_analysisPreferences->m_recordTarget &
          RecordTarget::RECORD_TO_FILE) == RecordTarget::RECORD_TO_FILE)
        {
          // Now check that the analysis file could be opened.

          if(mpa_analysisFile != nullptr)
            {
              delete mpa_analysisFile;
              mpa_analysisFile = nullptr;
            }

          mpa_analysisFile = new QFile(mpa_analysisPreferences->m_fileName);

          bool res = false;

          if(mpa_analysisPreferences->m_fileOpenMode == QIODevice::Truncate)

            // Interestingly, only truncate did not work. Since writeonly
            // implies truncate, that's fine, and it works.

            res = mpa_analysisFile->open(QIODevice::WriteOnly);
          else
            res = mpa_analysisFile->open(QIODevice::Append);

          if(!res)
            {
              delete mpa_analysisFile;
              mpa_analysisFile = nullptr;

              QMessageBox msgBox;
              msgBox.setText(
                "Failed to open the analysis file, please fix "
                "the analysis preferences.");
              msgBox.exec();

              return;
            }
          else
            {
              // Close the file, which will be opened each time it is
              // necessary.
              mpa_analysisFile->close();

              // We can effectively start the work. The windows needing to
              // access the file will call getAnalysisFile() and make sure
              // that it is not nullptr.
            }
        }
    }
  else // if(res == QDialog::Accepted)
    {
      // The dialog returned a rejected result. We cannot do anything.
    }
}


void
ProgramWindow::msRunDataSetRemovalRequested(
  MsRunDataSetCstSPtr &ms_run_data_set_csp)
{
  // qDebug() << "Removal of ms run data set:" << ms_run_data_set_csp.get()
  //<< "requested";

  // The user asks that a given ms run data set be removed from the software.
  // We need to orderly remove all the shared pointer references so that the
  // object is destroyed. Shared pointers to the ms run data set are located
  // in the plottables created on the basis of the ms run data set. We need to
  // remove from all the plot widgets, all the plottables that have been
  // created using the ms run data set as the starting point of integrations.

  // All the plottables are registered in the tree that mimicks the structure
  // of all the plottable and plot widget in the program.

  // Let's see the usage count of the MsRunDataSetCstSPtr.
  // qDebug() << "Usage count:" << ms_run_data_set_csp.use_count();

  // Find all the root nodes that match ms_run_data_set_csp non recursively.
  std::vector<DataPlottableNode *> found_nodes =
    m_dataPlottableTree.findNodes(ms_run_data_set_csp, false);

  // qDebug() << "Found nodes:" << found_nodes.size() << "nodes:";
  // for(std::size_t iter = 0; iter < found_nodes.size(); ++iter)
  // qDebug() << found_nodes.at(iter)->toString(0, true);

  // Since each node has both the plot widget and the plottable pointer, we
  // can ask for the recursive destruction of the plottable.

  // Let's see the usage count of the MsRunDataSetCstSPtr.
  // qDebug() << "Usage count:" << ms_run_data_set_csp.use_count();

  for(auto &&node : found_nodes)
    {
      // true: recursively
      // false: not destroying a widget.

      plottableDestructionRequested(
        node->getPlotWidget(), node->getPlottable(), true);
    }

  // Let's see the usage count of the MsRunDataSetCstSPtr.
  // qDebug() << "Usage count:" << ms_run_data_set_csp.use_count();

  // Look for a tree view window that matches the ms
  // run data set.

  std::map<MsRunDataSetCstSPtr, MsRunDataSetTableViewWnd *>::iterator
    res_iterator = std::find_if(
      mp_msRunDataSetTableViewWndMap.begin(),
      mp_msRunDataSetTableViewWndMap.end(),
      [ms_run_data_set_csp](
        const std::pair<MsRunDataSetCstSPtr, MsRunDataSetTableViewWnd *> item) {
        return item.first == ms_run_data_set_csp;
      });

  if(res_iterator == mp_msRunDataSetTableViewWndMap.end())
    {
      // Nothing to do, the data set had not associated tree view.
    }
  else
    {
      delete res_iterator->second;

      mp_msRunDataSetTableViewWndMap.erase(ms_run_data_set_csp);
    }

  // Finally remove the ms run data set item from the list widget in the
  // open ms run data set dialog window.

  mp_openMsRunDataSetsDlg->removeMsRunDataSet(ms_run_data_set_csp);

  // Let's see the usage count of the MsRunDataSetCstSPtr.
  // qDebug() << "Usage count:" << ms_run_data_set_csp.use_count();
}


const DataPlottableTree &
ProgramWindow::getDataPlottableTree() const
{
  return m_dataPlottableTree;
}


std::vector<MsRunDataSetCstSPtr>
ProgramWindow::allSelectedOrUniqueMsRunDataSet()
{
  return mp_openMsRunDataSetsDlg->allSelectedOrUniqueMsRunDataSets();
}


BaseTracePlotWnd *
ProgramWindow::getPlotWndPtr(const QString &type_name)
{
  if(type_name != "mass spectrum" && type_name != "tic xic chromatogram" &&
     type_name != "drift spectrum")
    qFatal("Error with the plot window type name.");

  if(type_name == "mass spectrum")
    return mp_massSpecPlotWnd;
  else if(type_name == "tic xic chromatogram")
    return mp_ticXicChromPlotWnd;
  else if(type_name == "drift spectrum")
    return mp_driftSpecPlotWnd;

  qFatal("Should never encounter this point.");

  return nullptr;
}


TaskMonitorWnd *
ProgramWindow::getTaskMonitorWnd() const
{
  return mp_taskMonitorWnd;
}


void
ProgramWindow::integrateToDt(
  QCPAbstractPlottable *parent_plottable_p,
  std::shared_ptr<QualifiedMassSpectraVector> qualified_mass_spectra_sp,
  const ProcessingFlow &processing_flow)
{
  // Each trace plot window handles the integration requests that produce
  // their own type of data, so we delegate the integration to the
  // relevant window.

  mp_driftSpecPlotWnd->integrateToDt(
    parent_plottable_p, qualified_mass_spectra_sp, processing_flow);
}


void
ProgramWindow::integrateToMz(
  QCPAbstractPlottable *parent_plottable_p,
  std::shared_ptr<QualifiedMassSpectraVector> qualified_mass_spectra_sp,
  const ProcessingFlow &processing_flow)
{
  // Each trace plot window handles the integration requests that produce
  // their own type of data, so we delegate the integration to the
  // relevant window.

  // qDebug().noquote() << "Integrating to mz with processing flow:"
  //<< processing_flow.toString();

  mp_massSpecPlotWnd->integrateToMz(
    parent_plottable_p, qualified_mass_spectra_sp, processing_flow);
}


void
ProgramWindow::integrateToRt(
  QCPAbstractPlottable *parent_plottable_p,
  std::shared_ptr<QualifiedMassSpectraVector> qualified_mass_spectra_sp,
  const ProcessingFlow &processing_flow)
{
  // Each trace plot window handles the integration requests that produce
  // their own type of data, so we delegate the integration to the
  // relevant window.

  // qDebug().noquote() << "Integrating to rt with processing flow:"
  // << processing_flow.toString();

  mp_ticXicChromPlotWnd->integrateToRt(
    parent_plottable_p, qualified_mass_spectra_sp, processing_flow);
}


void
ProgramWindow::integrateToTicIntensity(
  [[maybe_unused]] QCPAbstractPlottable *parent_plottable_p,
  std::shared_ptr<QualifiedMassSpectraVector> qualified_mass_spectra_sp,
  const ProcessingFlow &processing_flow)
{

  // Get the ms run data set that for the graph we are going to base the
  // integration on.
  MsRunDataSetCstSPtr ms_run_data_set_csp =
    processing_flow.getMsRunDataSetCstSPtr();
  if(ms_run_data_set_csp == nullptr)
    qFatal("Cannot be that the pointer is nullptr.");

  // Let's see the usage count of the MsRunDataSetCstSPtr.
  //  qDebug() << "Usage count:" << ms_run_data_set_sp.use_count();

  // Allocate the mass data integrator that is speciaized in the computation
  // of TIC chromaotograms, starting from a vector of qualified mass spectrum
  // pointers.

  QualifiedMassSpectrumVectorMassDataIntegratorToTicInt
    *mass_data_integrator_p =
      new QualifiedMassSpectrumVectorMassDataIntegratorToTicInt(
        ms_run_data_set_csp, processing_flow, qualified_mass_spectra_sp);

  mass_data_integrator_p->setMaxThreadUseCount(m_maxThreadUseCount);

  MassDataIntegratorTask *mass_data_integrator_task_p =
    new MassDataIntegratorTask();

  // This signal starts the computation in the MassDataIntegratorTask object.
  connect(
    this,
    static_cast<void (ProgramWindow::*)(
      QualifiedMassSpectrumVectorMassDataIntegratorToTicInt *)>(
      &ProgramWindow::integrateQualifiedMassSpectrumVectorToTicIntensitySignal),
    mass_data_integrator_task_p,
    static_cast<void (MassDataIntegratorTask::*)(
      QualifiedMassSpectrumVectorMassDataIntegratorToTicInt *)>(
      &MassDataIntegratorTask::integrateToTicIntensity),
    // Fundamental for signals that travel across QThread instances...
    Qt::QueuedConnection);

  // When the task finishes, it sends a signal that we trap to go on with
  // the plot widget creation stuff.

  connect(mass_data_integrator_task_p,
          static_cast<void (MassDataIntegratorTask::*)(
            QualifiedMassSpectrumVectorMassDataIntegrator *)>(
            &MassDataIntegratorTask::finishedIntegratingDataSignal),
          this,
          static_cast<void (ProgramWindow::*)(
            QualifiedMassSpectrumVectorMassDataIntegrator *)>(
            &ProgramWindow::
              finishedIntegratingQualifiedMassSpectrumVectorToTicIntensity),
          // Fundamental for signals that travel across QThread instances...
          Qt::QueuedConnection);

  // Allocate the thread in which the integrator task will run.
  QThread *thread_p = new QThread;

  // Move the task to the matching thread.
  mass_data_integrator_task_p->moveToThread(thread_p);
  thread_p->start();

  // Since we allocated the QThread dynamically we need to be able to destroy
  // it later, so make the connection.
  connect(mass_data_integrator_task_p,
          static_cast<void (MassDataIntegratorTask::*)(
            QualifiedMassSpectrumVectorMassDataIntegrator *)>(
            &MassDataIntegratorTask::finishedIntegratingDataSignal),
          this,
          [thread_p, mass_data_integrator_task_p]() {
            // Do not forget that we have to delete the MassDataIntegratorTask
            // allocated instance.
            mass_data_integrator_task_p->deleteLater();
            // Once the task has been labelled to be deleted later, we can
            // stop the thread and ask for it to also be deleted later.
            thread_p->deleteLater(), thread_p->quit();
            thread_p->wait();
          });

  // Ensure the mass data integrator messages are used.

  connect(
    mass_data_integrator_p,
    &QualifiedMassSpectrumVectorMassDataIntegrator::logTextToConsoleSignal,
    this,
    &ProgramWindow::logTextToConsole);

  // qDebug() << "the integrator pointer:" << mass_data_integrator_p;

  // Allocate a new TaskMonitorCompositeWidget that will receive all the
  // integrator's signals and provide feedback to the user about the ongoing
  // integration.

  TaskMonitorCompositeWidget *task_monitor_composite_widget_p =
    mp_taskMonitorWnd->addTaskMonitorWidget(Qt::red);

  // Initialize the monitor composite widget's widgets and make all the
  // connections mass data integrator <--> widget.

  task_monitor_composite_widget_p->setMsRunIdText(
    ms_run_data_set_csp->getMsRunId()->getSampleName());
  task_monitor_composite_widget_p->setTaskDescriptionText(
    "Integrating to TIC intensity from the table view");
  task_monitor_composite_widget_p->setProgressBarMinValue(0);

  // Make the connections

  // When the MsRunReadTask instance has finished working, it will send a
  // signal that we trap to finally destroy (after a time lag of some seconds,
  // the monitor widget.

  connect(mass_data_integrator_task_p,
          static_cast<void (MassDataIntegratorTask::*)(
            QualifiedMassSpectrumVectorMassDataIntegrator *)>(
            &MassDataIntegratorTask::finishedIntegratingDataSignal),
          task_monitor_composite_widget_p,
          &TaskMonitorCompositeWidget::taskFinished,
          Qt::QueuedConnection);

  // If the user clicks the cancel button, relay the signal to the loader.
  connect(task_monitor_composite_widget_p,
          &TaskMonitorCompositeWidget::cancelTaskSignal,
          mass_data_integrator_p,
          &QualifiedMassSpectrumVectorMassDataIntegrator::cancelOperation);

  task_monitor_composite_widget_p->makeMassDataIntegratorConnections(
    mass_data_integrator_p);

  emit integrateQualifiedMassSpectrumVectorToTicIntensitySignal(
    mass_data_integrator_p);

  // Because the emitter is the same ProgramWindow object, we need to
  // disconnect the signal so that at next opening of mass data file, it will
  // not be called twice.
  disconnect(
    this,
    static_cast<void (ProgramWindow::*)(
      QualifiedMassSpectrumVectorMassDataIntegratorToTicInt *)>(
      &ProgramWindow::integrateQualifiedMassSpectrumVectorToTicIntensitySignal),
    mass_data_integrator_task_p,
    static_cast<void (MassDataIntegratorTask::*)(
      QualifiedMassSpectrumVectorMassDataIntegratorToTicInt *)>(
      &MassDataIntegratorTask::integrateToTicIntensity));
}


void
ProgramWindow::integrateToDtMz(
  QCPAbstractPlottable *parent_plottable_p,
  std::shared_ptr<std::vector<pappso::QualifiedMassSpectrumCstSPtr>>
    qualified_mass_spectra_sp,
  const ProcessingFlow &processing_flow)
{
  mp_driftSpecMassSpecColorMapWnd->integrateToDtMz(
    parent_plottable_p, qualified_mass_spectra_sp, processing_flow);
}


void
ProgramWindow::integrateToDtRt(
  QCPAbstractPlottable *parent_plottable_p,
  std::shared_ptr<std::vector<pappso::QualifiedMassSpectrumCstSPtr>>
    qualified_mass_spectra_sp,
  const ProcessingFlow &processing_flow)
{
  mp_ticXicChromDriftSpecColorMapWnd->integrateToDtRt(
    parent_plottable_p, qualified_mass_spectra_sp, processing_flow);
}


void
ProgramWindow::integrateToMzRt(
  QCPAbstractPlottable *parent_plottable_p,
  std::shared_ptr<std::vector<pappso::QualifiedMassSpectrumCstSPtr>>
    qualified_mass_spectra_sp,
  const ProcessingFlow &processing_flow)
{
  mp_ticXicChromMassSpecColorMapWnd->integrateToMzRt(
    parent_plottable_p, qualified_mass_spectra_sp, processing_flow);
}


void
ProgramWindow::xicIntegrationToRt(const ProcessingFlow &processing_flow)
{
  // qDebug().noquote() << "XIC integration to rt with processing flow:"
  //<< processing_flow.toString();

  // Sanity check:

  if(processing_flow.getMsRunDataSetCstSPtr() == nullptr)
    qFatal("Cannot be that the pointer is nullptr.");

  MsRunDataSetCstSPtr ms_run_data_set_csp =
    processing_flow.getMsRunDataSetCstSPtr();

  // Pass the integrator the flow we got as param and that describes in its
  // most recent step the integration that it should perform.
  MsRunDataSetTreeMassDataIntegratorToRt *mass_data_integrator_p =
    new MsRunDataSetTreeMassDataIntegratorToRt(ms_run_data_set_csp,
                                               processing_flow);

  // Ensure the mass data integrator messages are used.

  connect(mass_data_integrator_p,
          &MassDataIntegrator::logTextToConsoleSignal,
          this,
          &ProgramWindow::logTextToConsole);

  // qDebug() << "the integrator pointer:" << mass_data_integrator_p;

  // Allocate a mass data integrator to integrate the data.

  MassDataIntegratorTask *mass_data_integrator_task_p =
    new MassDataIntegratorTask();

  // This signal starts the computation in the MassDataIntegratorTask object.
  connect(
    this,
    // SIGNAL(integrateToRtSignal(MsRunDataSetTreeMassDataIntegratorToRt *)),
    static_cast<void (ProgramWindow::*)(
      MsRunDataSetTreeMassDataIntegratorToRt *)>(
      &ProgramWindow::integrateToRtSignal),
    mass_data_integrator_task_p,
    // SLOT(integrateToRt(MsRunDataSetTreeMassDataIntegratorToRt *)),
    static_cast<void (MassDataIntegratorTask::*)(
      MsRunDataSetTreeMassDataIntegratorToRt *)>(
      &MassDataIntegratorTask::integrateToRt),
    // Fundamental for signals that travel across QThread instances...
    Qt::QueuedConnection);

  // Allocate the thread in which the integrator task will run.
  QThread *thread_p = new QThread;

  // Move the task to the matching thread.
  mass_data_integrator_task_p->moveToThread(thread_p);
  thread_p->start();

  // When the read task finishes, it sends a signal that we trap to go on with
  // the plot widget creation stuff.

  // Since we allocated the QThread dynamically we need to be able to destroy
  // it later, so make the connection.
  connect(
    mass_data_integrator_task_p,
    static_cast<void (MassDataIntegratorTask::*)(MassDataIntegrator *)>(
      &MassDataIntegratorTask::finishedIntegratingDataSignal),
    this,
    [this, thread_p, mass_data_integrator_p, mass_data_integrator_task_p]() {
      // Do not forget that we have to delete the MassDataIntegratorTask
      // allocated instance.
      mass_data_integrator_task_p->deleteLater();
      // Once the task has been labelled to be deleted later, we can stop
      // the thread and ask for it to also be deleted later.
      thread_p->deleteLater(), thread_p->quit();
      thread_p->wait();
      this->finishedXicIntegrationToRt(mass_data_integrator_p);
    });


  // Allocate a new TaskMonitorCompositeWidget that will receive all the
  // integrator's signals and provide feedback to the user about the ongoing
  // integration.

  TaskMonitorCompositeWidget *task_monitor_composite_widget_p =
    getTaskMonitorWnd()->addTaskMonitorWidget(Qt::red);

  // Initialize the monitor composite widget's widgets and make all the
  // connections mass data integrator <--> widget.

  task_monitor_composite_widget_p->setMsRunIdText(
    ms_run_data_set_csp->getMsRunId()->getSampleName());
  task_monitor_composite_widget_p->setTaskDescriptionText(
    "Integrating to XIC chromatogram.");
  task_monitor_composite_widget_p->setProgressBarMinValue(0);

  // Make the connections

  // When the integrator task instance has finished working, it will send a
  // signal that we trap to finally destroy (after a time lag of some seconds,
  // the monitor widget.

  connect(mass_data_integrator_task_p,
          static_cast<void (MassDataIntegratorTask::*)(MassDataIntegrator *)>(
            &MassDataIntegratorTask::finishedIntegratingDataSignal),
          task_monitor_composite_widget_p,
          &TaskMonitorCompositeWidget::taskFinished,
          Qt::QueuedConnection);

  // If the user clicks the cancel button, relay the signal to the loader.
  connect(task_monitor_composite_widget_p,
          &TaskMonitorCompositeWidget::cancelTaskSignal,
          mass_data_integrator_p,
          &MassDataIntegrator::cancelOperation);

  // We need to register the meta type for std::size_t because otherwise it
  // cannot be shipped though signals.

  qRegisterMetaType<std::size_t>("std::size_t");

  // Now make all the connections that will allow the integrator to provide
  // dynamic feedback to the user via the task monitor widget.
  task_monitor_composite_widget_p->makeMassDataIntegratorConnections(
    mass_data_integrator_p);

  // qDebug() << "going to emit integrateToRtSignal with mass "
  //"data integrator:"
  //<< mass_data_integrator_p;

  emit integrateToRtSignal(mass_data_integrator_p);

  // We do not want to make signal/slot calls more than once. This is because
  // one user might well trigger more than one integration from this window to
  // a mass spectrum. Thus we do not want that *this window be still connected
  // to the specific mass_data_integrator_task_p when a new integration is
  // triggered. We want the signal/slot pairs to be contained to specific
  // objects. Each MassSpecTracePlotWnd::integrateToMz() call must be
  // contained to a this/mass_data_integrator_task_p specific signal/slot
  // pair.
  disconnect(
    this,
    // SIGNAL(integrateToRtSignal(MsRunDataSetTreeMassDataIntegratorToRt *)),
    static_cast<void (ProgramWindow::*)(
      MsRunDataSetTreeMassDataIntegratorToRt *)>(
      &ProgramWindow::integrateToRtSignal),
    mass_data_integrator_task_p,
    // SLOT(integrateToRt(MsRunDataSetTreeMassDataIntegratorToRt *)));
    static_cast<void (MassDataIntegratorTask::*)(
      MsRunDataSetTreeMassDataIntegratorToRt *)>(
      &MassDataIntegratorTask::integrateToRt));
}


void
ProgramWindow::displayMassSpectralTrace(pappso::Trace trace,
                                        const ProcessingFlow &processing_flow,
                                        const QString &sample_name)
{
  // qDebug() << "The trace has size:" << trace.size()
  //<< "and sample name:" << sample_name;

  // There are two possibilities:
  //
  // 1. There is not a single MS run data set: we need to create one as a shim
  // 2. There is/are at least one MS run data set: ask the user if they want
  // to anchor the new to one of these by providing a selection list.

  // Check with the OpenMsRunDataSetsDlg if there is at least one MsRunDataSet
  // available.

  // Set the pointer to nullptr, that will help us later.
  MsRunDataSetCstSPtr ms_run_data_set_csp = nullptr;

  QColor color;

  bool ok = false;

  QString local_sample_name;

  std::size_t opened_ms_run_data_sets =
    mp_openMsRunDataSetsDlg->msRunDataSetCount();

  if(opened_ms_run_data_sets)
    {

      // There is a least one MS run data set. Provide a list to the user and
      // let them choose one of these. There might also be a choice to create
      // a shim file.

      // Get a list of the MS run data sets.
      std::vector<MsRunDataSetCstSPtr> ms_run_data_sets =
        mp_openMsRunDataSetsDlg->allMsRunDataSets();

      // qDebug() << "MS run data sets count:" << ms_run_data_sets.size();

      // The list of items' sample names will start with an item
      // allowing the user to ask for the creation of a new MS run data set
      // for the trace to be displayed.
      QStringList item_texts = {"Create a new MS run data set"};

      // Extract a list of strings from that list of widgets.
      for(auto &&item : ms_run_data_sets)
        {
          local_sample_name = item->getMsRunId()->getSampleName();
          // qDebug() << "local_sample_name:" << local_sample_name;

          item_texts.append(local_sample_name);
        }

      // qDebug() << "All the sample names:" << item_texts;

      // Craft an input dialog.

      QInputDialog input_dialog(this);
      input_dialog.setComboBoxItems(item_texts);

      // Now as the user to select one item.

      QString item_text = QInputDialog::getItem(this,
                                                "Select a MS run data set",
                                                "MS run data set:",
                                                item_texts,
                                                0,
                                                false,
                                                &ok);
      if(!ok)
        {
          // The user pressed cancel. Just return.
          return;
        }

      if(item_text != "Create a new MS run data set")
        {
          // Now we know that the user wants the new trace to be anchored to a
          // given MsRunDataSet.

          ok = false;
          ok = mp_openMsRunDataSetsDlg->msRunDataSetFromSampleName(
            item_text, ms_run_data_set_csp, color);

          if(!ok || ms_run_data_set_csp == nullptr)
            qFatal("Programming error.");

          // At this point we know we won't need to create a shim ms run data
          // set because ms_run_data_set_csp is no more nullptr and we also
          // know the color.
        }
      else
        {
          // qDebug() << "Creating a new MS run data set.";
        }
    }
  // End of
  // if(opened_ms_run_data_sets)

  // Now test if we need to create a shim ms run data set or not by checking
  // ms_run_data_set_csp

  if(ms_run_data_set_csp == nullptr)
    {

      // Craft a local sample name

      if(sample_name.isEmpty())
        {
          QDateTime current_date_time = QDateTime::currentDateTime();

          QString time_as_string = current_date_time.toString("yyyyMMdd-HHmm");

          local_sample_name = "mass-spectrum-" + time_as_string;
        }
      else
        local_sample_name = sample_name;

      openMassSpectrometryFileFromText(trace.toString(), local_sample_name);

      // At this point we should have a new file. When the new ms run data set
      // will be added to the open ms run data sets dlg list widget, it will
      // be selected.

      showOpenMsRunDataSetsWnd();

      QMessageBox::information(this,
                               "Select a MS run data set",
                               "One shim mass data file was loaded.\n"
                               "Please, perform the task agin and select the "
                               "right MS run data set.");

      return;
    }

  // At this point we have what we needed, a proper ms run data set pointer!

  ProcessingFlow local_processing_flow(processing_flow);

  local_processing_flow.setMsRunDataSetCstSPtr(ms_run_data_set_csp);

  mp_massSpecPlotWnd->addTracePlot(
    trace, ms_run_data_set_csp, local_processing_flow, color, nullptr);
}


const BasePlotCompositeWidget *
ProgramWindow::getPlotWidget(
  [[maybe_unused]] MsRunDataSetCstSPtr ms_run_data_set_csp,
  QCPAbstractPlottable *plottable_p)
{
  return m_dataPlottableTree.getPlotWidget(plottable_p);
}


void
ProgramWindow::documentMsRunDataPlottableFiliation(
  MsRunDataSetCstSPtr ms_run_data_set_csp,
  QCPAbstractPlottable *new_plottable_p,
  QCPAbstractPlottable *parent_plottable_p,
  BasePlotCompositeWidget *plot_widget_p)
{
  // qDebug();

  // Let's see the usage count of the MsRunDataSetCstSPtr.
  // qDebug() << "Usage count:" << ms_run_data_set_csp.use_count();

  // Now we need to document the filiation of the parent / child
  // plottable.

  // qDebug() << "Before documenting: data plottable tree has"
  //<< m_dataPlottableTree.depth() << "depth and"
  //<< m_dataPlottableTree.size() << "size";

  m_dataPlottableTree.addPlottable(
    ms_run_data_set_csp, new_plottable_p, parent_plottable_p, plot_widget_p);

  // Let's see the usage count of the MsRunDataSetCstSPtr.
  // qDebug() << "Usage count:" << ms_run_data_set_csp.use_count();

  // qDebug() << "After documenting data plot plottable tree has"
  //<< m_dataPlottableTree.depth() << "depth and"
  //<< m_dataPlottableTree.size() << "size";
}


void
ProgramWindow::plottableDestructionRequested(
  BasePlotCompositeWidget *base_plot_composite_widget_p,
  QCPAbstractPlottable *plottable_p,
  bool recursively)
{
  // qDebug().noquote() << "Destruction of plottable:"
  //<< pappso::Utils::pointerToString(plottable_p)
  //<< "is being requested recursively:" << recursively;

  MsRunDataSetCstSPtr ms_run_data_set_csp =
    base_plot_composite_widget_p->getMsRunDataSetCstSPtrForPlottable(
      plottable_p);

  // Let's see the usage count of the MsRunDataSetCstSPtr.
  // qDebug() << "Usage count:" << ms_run_data_set_csp.use_count();

  // When one plottable is on the verge of being deleted, we need to first
  // check its node in the plottable data tree. This is because we need to
  // at least remove that node !

  // Further, there is another fact to keep in mind: if the user wanted to
  // remove all the children plottable, then we would need to do that.

  // Another complexity: if the plottable that is removed had children,
  // but the user does not want to remove them, we would need to reparent
  // them to the parent of the plottable being deleted !

  // How many children did that plottable have?

  // qDebug() << "Getting the right node for the plottable to be destroyed.";

  DataPlottableNode *plottable_node_p =
    m_dataPlottableTree.findNode(plottable_p);

  if(plottable_node_p == nullptr)
    qFatal(
      "Could not find a node for plottable . This is a programming "
      "error.");

  // qDebug() << "The plottable is in node:"
  //<< plottable_node_p->toString(0, true);

  std::size_t descendant_count = 0;
  plottable_node_p->size(descendant_count);

  // qDebug() << "The plottable node has" << descendant_count <<
  // "descendants";

  // bool res =
  m_dataPlottableTree.removeNodeAndPlottable(plottable_node_p, recursively);

  // qDebug() << "Removal of node(s) and plottable(s) is success:" << res;

  // Let's see the usage count of the MsRunDataSetCstSPtr.
  //  qDebug() << "Usage count:" << ms_run_data_set_csp.use_count();
}


void
ProgramWindow::plottableDestructionRequested(
  BasePlotCompositeWidget *base_plot_composite_widget_p,
  QCPAbstractPlottable *plottable_p,
  const pappso::BasePlotContext &context)
{
  bool recursively = context.m_keyboardModifiers & Qt::ControlModifier;

  // If a recursive plottable destruction is requested, the the plottable
  // and all its children, as documented by the main program's plottable
  // tree nodes are to be destroyed.

  // if(recursively)
  // qDebug() << "Destruction recursive of plottable:" << plottable_p
  //<< "is being requested.";
  // else
  // qDebug() << "Destruction of plottable:" << plottable_p
  //<< "is being requested.";

  return plottableDestructionRequested(
    base_plot_composite_widget_p, plottable_p, recursively);
}


void
ProgramWindow::plotCompositeWidgetDestructionRequested(
  BasePlotCompositeWidget *base_plot_composite_widget_p)
{
  // qDebug() << "Handling the destruction of the plot widget:"
  //<< base_plot_composite_widget_p;

  // One plot widget and all the plottables that it contains need to be
  // destroyed. We first need to get a list of all the data plot tree nodes
  // that match the plot widget. Then we non-recursively destroy all the
  // plottables and matching data tree nodes. Finally we destroy the widget.

  std::vector<DataPlottableNode *> plottable_nodes =
    m_dataPlottableTree.findNodes(base_plot_composite_widget_p, true);

  // qDebug() << "The number of nodes for plot widget:"
  //<< base_plot_composite_widget_p << "is:" << plottable_nodes.size();

  // We want an orderly destruction of the plottable(s).

  for(auto &&node_p : plottable_nodes)
    {

      plottableDestructionRequested(
        base_plot_composite_widget_p, node_p->getPlottable(), false);
    }

  // Once all the plottables have been destroyed, we need to destroy the plot
  // widget itself.

  delete base_plot_composite_widget_p;

  // qDebug() << "Done deleting the composite widget.";
}


void
ProgramWindow::seedInitialTicChromatogramAndMsRunDataSetStatistics(
  MsRunDataSetSPtr &ms_run_data_set_sp)
{
  if(ms_run_data_set_sp == nullptr)
    qFatal("Cannot be that pointer is nullptr");

  // qDebug() << "sample name:"
  //<< ms_run_data_set_sp->getMsRunId()->getSampleName();

  // Let's see the usage count of the MsRunDataSetCstSPtr.
  //  qDebug() << "Usage count:" << ms_run_data_set_sp.use_count();

  MassDataIntegratorTask *mass_data_integrator_task_p =
    new MassDataIntegratorTask();

  // This signal starts the computation in the MassDataIntegratorTask object.
  connect(
    this,
    &ProgramWindow::seedInitialTicChromatogramAndMsRunDataSetStatisticsSignal,
    mass_data_integrator_task_p,
    &MassDataIntegratorTask::
      seedInitialTicChromatogramAndMsRunDataSetStatistics,
    // Fundamental for signals that travel across QThread instances...
    Qt::QueuedConnection);

  // When the task finishes, it sends a signal that we trap to go on with
  // the plot widget creation stuff.

  connect(mass_data_integrator_task_p,
          static_cast<void (MassDataIntegratorTask::*)(MassDataIntegrator *)>(
            &MassDataIntegratorTask::finishedIntegratingDataSignal),
          this,
          &ProgramWindow::
            finishedSeedingInitialTicChromatogramAndMsRunDataSetStatistics,
          // Fundamental for signals that travel across QThread instances...
          Qt::QueuedConnection);

  // Allocate the thread in which the integrator task will run.
  QThread *thread_p = new QThread;

  // Move the task to the matching thread.
  mass_data_integrator_task_p->moveToThread(thread_p);
  thread_p->start();

  // Since we allocated the QThread dynamically we need to be able to destroy
  // it later, so make the connection.
  connect(mass_data_integrator_task_p,
          static_cast<void (MassDataIntegratorTask::*)(MassDataIntegrator *)>(
            &MassDataIntegratorTask::finishedIntegratingDataSignal),
          this,
          [thread_p, mass_data_integrator_task_p]() {
            // Do not forget that we have to delete the MassDataIntegratorTask
            // allocated instance.
            mass_data_integrator_task_p->deleteLater();
            // Once the task has been labelled to be deleted later, we can
            // stop the thread and ask for it to also be deleted later.
            thread_p->deleteLater(), thread_p->quit();
            thread_p->wait();
          });

  // Let's see the usage count of the MsRunDataSetCstSPtr.
  //  qDebug() << "Usage count:" << ms_run_data_set_sp.use_count();

  // No need to provide any processing flow object to the constructor, as we
  // have none yet, this is the very first integration.
  MsRunDataSetTreeMassDataIntegratorToRt *mass_data_integrator_p =
    new MsRunDataSetTreeMassDataIntegratorToRt(ms_run_data_set_sp);

  // Let's see the usage count of the MsRunDataSetCstSPtr.
  // qDebug() << "Usage count:" << ms_run_data_set_sp.use_count();

  // Ensure the mass data integrator messages are used.

  connect(mass_data_integrator_p,
          &MassDataIntegrator::logTextToConsoleSignal,
          this,
          &ProgramWindow::logTextToConsole);

  // qDebug() << "the integrator pointer:" << mass_data_integrator_p;

  // Allocate a new TaskMonitorCompositeWidget that will receive all the
  // integrator's signals and provide feedback to the user about the ongoing
  // integration.

  TaskMonitorCompositeWidget *task_monitor_composite_widget_p =
    mp_taskMonitorWnd->addTaskMonitorWidget(Qt::red);

  // Initialize the monitor composite widget's widgets and make all the
  // connections mass data integrator <--> widget.

  task_monitor_composite_widget_p->setMsRunIdText(
    ms_run_data_set_sp->getMsRunId()->getSampleName());
  task_monitor_composite_widget_p->setTaskDescriptionText(
    "Integrating to initial TIC chromatogram and calculating MS run "
    "statistics");
  task_monitor_composite_widget_p->setProgressBarMinValue(0);

  // Make the connections

  // When the MsRunReadTask instance has finished working, it will send a
  // signal that we trap to finally destroy (after a time lag of some seconds,
  // the monitor widget.

  connect(mass_data_integrator_task_p,
          static_cast<void (MassDataIntegratorTask::*)(MassDataIntegrator *)>(
            &MassDataIntegratorTask::finishedIntegratingDataSignal),
          task_monitor_composite_widget_p,
          &TaskMonitorCompositeWidget::taskFinished,
          Qt::QueuedConnection);

  // If the user clicks the cancel button, relay the signal to the loader.
  connect(task_monitor_composite_widget_p,
          &TaskMonitorCompositeWidget::cancelTaskSignal,
          mass_data_integrator_p,
          &MassDataIntegrator::cancelOperation);

  // We need to register the meta type for std::size_t because otherwise it
  // cannot be shipped though signals.

  qRegisterMetaType<std::size_t>("std::size_t");

  task_monitor_composite_widget_p->makeMassDataIntegratorConnections(
    mass_data_integrator_p);

  // qDebug() << "going to emit integrateToInitialTicChromatogramSignal with
  // mass " "data integrator:"
  //<< mass_data_integrator_p;

  emit seedInitialTicChromatogramAndMsRunDataSetStatisticsSignal(
    mass_data_integrator_p);

  // Because the emitter is the same ProgramWindow object, we need to
  // disconnect the signal so that at next opening of mass data file, it will
  // not be called twice.
  disconnect(
    this,
    &ProgramWindow::seedInitialTicChromatogramAndMsRunDataSetStatisticsSignal,
    mass_data_integrator_task_p,
    &MassDataIntegratorTask::
      seedInitialTicChromatogramAndMsRunDataSetStatistics);
}


void
ProgramWindow::finishedSeedingInitialTicChromatogramAndMsRunDataSetStatistics(
  MassDataIntegrator *mass_data_integrator_p)
{
  qDebug();

  // In this calculation, the m_mapTrace object in the integrator is the TIC
  // chroamtogram.

  // The function below uses the mass_data_integrator_p to get to the TIC
  // chromatogram and create the TIC chrom widget.
  std::pair<MsRunDataSetCstSPtr, QColor> result_pair =
    mp_ticXicChromPlotWnd->finishedIntegratingToInitialTicChromatogram(
      static_cast<MsRunDataSetTreeMassDataIntegratorToRt *>(
        mass_data_integrator_p));

  // Make sure the tic chrom wnd is visible!
  showTicXicChromatogramsWnd();

  // qDebug() << "the sample name:"
  //<< result_pair.first->getMsRunId()->getSampleName();

  if(result_pair.first != nullptr)
    {
      // There were data in the data set ! Go on.

      mp_openMsRunDataSetsDlg->newOpenMsRunDataSet(result_pair.first,
                                                   result_pair.second);

      showOpenMsRunDataSetsWnd();
    }

  // As for the statistics of the ms run data set, it has been set
  // to the data set already.
}


void
ProgramWindow::finishedIntegratingQualifiedMassSpectrumVectorToTicIntensity(
  QualifiedMassSpectrumVectorMassDataIntegrator *mass_data_integrator_p)
{
  //  qDebug();

  MsRunDataSetCstSPtr ms_run_data_set_csp =
    mass_data_integrator_p->getMsRunDataSet();

  pappso::Trace trace = mass_data_integrator_p->getMapTrace().toTrace();

  ProcessingFlow processing_flow = mass_data_integrator_p->getProcessingFlow();

  // We now need to display the result into the status bar of the window.

  double tic_intensity =
    static_cast<QualifiedMassSpectrumVectorMassDataIntegratorToTicInt *>(
      mass_data_integrator_p)
      ->getTicIntensity();

  emit(ticIntensityValueSignal(tic_intensity));

  QString sample_name = ms_run_data_set_csp->getMsRunId()->getSampleName();

  QString msg =
    QString("%1: TIC intensity: %2\n").arg(sample_name).arg(tic_intensity);

  QColor color =
    mp_openMsRunDataSetsDlg->colorForMsRunDataSet(ms_run_data_set_csp);

  logColoredTextToConsole(msg, color);

  // Finally destroy the integrator!
  delete mass_data_integrator_p;
}


void
ProgramWindow::finishedXicIntegrationToRt(
  MassDataIntegrator *mass_data_integrator_p)
{
  // qDebug();

  // At this point we need to ask the TIC XIC chromatogram window to display
  // the result of the compuation.

  MsRunDataSetCstSPtr ms_run_data_set_csp =
    mass_data_integrator_p->getMsRunDataSet();

  pappso::Trace trace = mass_data_integrator_p->getMapTrace().toTrace();

  ProcessingFlow processing_flow = mass_data_integrator_p->getProcessingFlow();

  QColor color =
    mp_openMsRunDataSetsDlg->colorForMsRunDataSet(ms_run_data_set_csp);

  mp_ticXicChromPlotWnd->newTrace(
    trace, ms_run_data_set_csp, processing_flow, color, nullptr);

  if(!mp_ticXicChromPlotWnd->isVisible())
    mp_ticXicChromPlotWnd->showWindow();

  // Finally destroy the integrator!
  delete mass_data_integrator_p;
}


void
ProgramWindow::logTextToConsole(QString msg)
{
  mp_consoleWnd->logTextToConsole(msg);
}


void
ProgramWindow::logColoredTextToConsole(QString text, const QColor &color)
{
  mp_consoleWnd->logColoredTextToConsole(text, color);
}


QColor
ProgramWindow::getColorForMsRunDataSet(
  MsRunDataSetCstSPtr ms_run_data_set_csp) const
{
  return mp_openMsRunDataSetsDlg->colorForMsRunDataSet(ms_run_data_set_csp);
}


QFile *
ProgramWindow::getAnalysisFilePtr()
{
  return mpa_analysisFile;
}


AnalysisPreferences *
ProgramWindow::getAnalysisPreferences()
{
  return mpa_analysisPreferences;
}


void
ProgramWindow::recordAnalysisStanza(QString stanza, const QColor &color)
{
  // Check that the user configured the analysis preferences.

  if(mpa_analysisPreferences == nullptr)
    return;

  // The record might go to a file or to the console, to the clipboard...
  if((mpa_analysisPreferences->m_recordTarget & RecordTarget::RECORD_TO_FILE) ==
     RecordTarget::RECORD_TO_FILE)
    {
      if(mpa_analysisFile != nullptr)
        {

          // Write the stanza, that was crafted by the calling plot widget to
          // the file.

          if(!mpa_analysisFile->open(QIODevice::Append))
            {
              statusBar()->showMessage(
                QString("Could not record the step because "
                        "the file could not be opened."),
                4000);
            }
          else
            {
              mpa_analysisFile->write(stanza.toLatin1());

              // Force writing because we may want to have tail -f work fine
              // on the file, and see modifications live to change fiels in a
              // text editor.

              mpa_analysisFile->flush();
              mpa_analysisFile->close();
            }
        }
      else
        {
          // qDebug() << "The mpa_analysisFile pointer is nullptr.";

          statusBar()->showMessage(
            QString("Could not record the analysis step to file. "
                    "Please define a file to write the data to."),
            4000);
        }
    }

  // Also, if recording to the console is asked for, then do that also.
  if((mpa_analysisPreferences->m_recordTarget &
      RecordTarget::RECORD_TO_CONSOLE) == RecordTarget::RECORD_TO_CONSOLE)
    {
      logColoredTextToConsole(stanza, color);
    }

  // Also, if recording to the clipboard is asked for, then do that also.
  if((mpa_analysisPreferences->m_recordTarget &
      RecordTarget::RECORD_TO_CLIPBOARD) == RecordTarget::RECORD_TO_CLIPBOARD)
    {
      QClipboard *clipboard = QApplication::clipboard();
      clipboard->setText(clipboard->text() + stanza, QClipboard::Clipboard);
    }

  return;
}


void
ProgramWindow::writeSettings()
{
  QSettings settings(static_cast<Application *>(QCoreApplication::instance())
                       ->getUserConfigSettingsFilePath(),
                     QSettings::IniFormat);
  settings.beginGroup("ProgramWindow");

  settings.setValue("geometry", saveGeometry());

  settings.endGroup();
}


void
ProgramWindow::readSettings()
{
  QSettings settings(static_cast<Application *>(QCoreApplication::instance())
                       ->getUserConfigSettingsFilePath(),
                     QSettings::IniFormat);
  settings.beginGroup("ProgramWindow");

  restoreGeometry(settings.value("geometry").toByteArray());

  settings.endGroup();
}


void
ProgramWindow::initializeAllWindows()
{
  mp_openMsRunDataSetsDlg = new OpenMsRunDataSetsDlg(this);
  // mp_openMsRunDataSetsDlg->show();

  mp_ticXicChromPlotWnd = new TicXicChromTracePlotWnd(
    this, "TIC/XIC chromatograms", "TicXicChromTracePlotWnd");
  // mp_ticXicChromPlotWnd->show();

  mp_massSpecPlotWnd =
    new MassSpecTracePlotWnd(this, "Mass spectra", "MassSpecTracePlotWnd");
  // mp_massSpecPlotWnd->show();

  connect(mp_massSpecPlotWnd,
          &BasePlotWnd::massDataToBeServedSignal,
          this,
          &ProgramWindow::massDataToBeServed);

  mp_driftSpecPlotWnd =
    new DriftSpecTracePlotWnd(this, "Drift spectra", "DriftSpecTracePlotWnd");
  // mp_driftSpecPlotWnd->show();

  mp_ticXicChromMassSpecColorMapWnd =
    new TicXicChromMassSpecColorMapWnd(this,
                                       "TIC/XIC chrom. / Mass spec. color maps",
                                       "TicXicChromMassSpecColorMapWnd");
  // mp_ticXicChromMassSpecColorMapWnd->show();

  mp_ticXicChromDriftSpecColorMapWnd = new TicXicChromDriftSpecColorMapWnd(
    this,
    "TIC/XIC chrom. / Drift spec. color maps",
    "TicXicChromDriftSpecColorMapWnd");
  // mp_ticXicChromDriftSpecColorMapWnd->show();

  mp_driftSpecMassSpecColorMapWnd =
    new DriftSpecMassSpecColorMapWnd(this,
                                     "Drift spec. / Mass spec. color maps",
                                     "DriftSpecMassSpecColorMapWnd");
  // mp_driftSpecMassSpecColorMapWnd->show();

  mp_xicExtractionWnd = new XicExtractionWnd(
    this, "XIC chromatogram extraction", "XicExtractionWnd");

  mp_taskMonitorWnd =
    new TaskMonitorWnd(this, "Task monitors", "TaskMonitorWnd");
  // mp_taskMonitorWnd->show();

  mp_consoleWnd = new ConsoleWnd(this, m_applicationName);
  // mp_consoleWnd->show();

  // NO, this one is "on demand".
  // mp_isotopicClusterGeneratorDlg = new IsoSpecDlg(this, m_applicationName);

  // NO, this one is "on demand".
  // mp_isotopicClusterShaperDlg = new MassPeakShaperDlg(this,
  // m_applicationName);

  // At this point, try to check if we should remind the user to
  // cite the paper.

  QSettings settings(static_cast<Application *>(QCoreApplication::instance())
                       ->getUserConfigSettingsFilePath(),
                     QSettings::IniFormat);

  settings.beginGroup("Globals");

  int run_count = settings.value("run_count", 0).toInt();

  ++run_count;

  if(run_count == 15)
    {
      AboutDlg *dlg = showAboutDlg();

      dlg->showHowToCiteTab();

      settings.setValue("run_count", 0);
    }
  else
    {
      settings.setValue("run_count", run_count);
    }

  settings.endGroup();
}


void
ProgramWindow::createMenusAndActions()
{

  // File menu
  mp_fileMenu = menuBar()->addMenu("&File");

  mp_openFullMsFileAct =
    new QAction("Open mass spectrum file(s) fully in &memory",
                dynamic_cast<QObject *>(this));
  mp_openFullMsFileAct->setStatusTip("Open mass data file(s) fully in memory");
  mp_openFullMsFileAct->setShortcut(QKeySequence("Ctrl+O, M"));

  connect(mp_openFullMsFileAct, &QAction::triggered, [this]() {
    openMassSpectrometryFileDlg(m_lastUsedDirectory, true);
  });

  mp_fileMenu->addAction(mp_openFullMsFileAct);

  mp_openStreamedMsFileAct =
    new QAction("Open mass spectrum file(s) in &streamed mode",
                dynamic_cast<QObject *>(this));
  mp_openStreamedMsFileAct->setStatusTip(
    "Open mass data file(s) in streamed mode");
  mp_openStreamedMsFileAct->setShortcut(QKeySequence("Ctrl+O, S"));

  connect(mp_openStreamedMsFileAct, &QAction::triggered, [this]() {
    openMassSpectrometryFileDlg(m_lastUsedDirectory, false);
  });

  mp_fileMenu->addAction(mp_openStreamedMsFileAct);

  QAction *open_ms_file_from_clipboard_act_p = new QAction(
    "Load mass spectrum from &clipboard", dynamic_cast<QObject *>(this));
  open_ms_file_from_clipboard_act_p->setStatusTip(
    "Load a mass spectrum directly from the clipboard");
  open_ms_file_from_clipboard_act_p->setShortcut(QKeySequence("Ctrl+L, C"));

  connect(open_ms_file_from_clipboard_act_p, &QAction::triggered, [this]() {
    openMassSpectrometryFileFromClipBoard();
  });

  mp_fileMenu->addAction(open_ms_file_from_clipboard_act_p);

  mp_fileMenu->addSeparator();

  QAction *analysis_preferences_act_p =
    new QAction("&Analysis preferences...", dynamic_cast<QObject *>(this));
  analysis_preferences_act_p->setStatusTip(tr("Set the analysis preferences"));
  analysis_preferences_act_p->setShortcut(QKeySequence("Ctrl+A, P"));

  connect(analysis_preferences_act_p, &QAction::triggered, [this]() {
    openAnalysisPreferencesDlg();
  });
  mp_fileMenu->addAction(analysis_preferences_act_p);

  mp_fileMenu->addSeparator();

  mp_quitAct = new QAction(tr("&Quit"), dynamic_cast<QObject *>(this));
  mp_quitAct->setStatusTip(tr("Exit mineXpert2"));
  mp_quitAct->setShortcut(tr("Ctrl+Q"));

  connect(mp_quitAct, &QAction::triggered, this, &ProgramWindow::close);
  mp_fileMenu->addAction(mp_quitAct);

  // Windows menu
  mp_windowsMenu = menuBar()->addMenu("&Windows");

  QAction *action_p = new QAction("Show &TIC/XIC chromatograms window",
                                  dynamic_cast<QObject *>(this));
  action_p->setShortcut(QKeySequence("Ctrl+W, R"));
  action_p->setStatusTip("Show the TIC/XIC chromatograms window");
  connect(action_p,
          &QAction::triggered,
          this,
          &ProgramWindow::showTicXicChromatogramsWnd);
  mp_windowsMenu->addAction(action_p);


  action_p =
    new QAction("Show &mass spectra window", dynamic_cast<QObject *>(this));
  action_p->setShortcut(QKeySequence("Ctrl+W, M"));
  action_p->setStatusTip("Show the mass spectra window");
  connect(
    action_p, &QAction::triggered, this, &ProgramWindow::showMassSpectraWnd);
  mp_windowsMenu->addAction(action_p);


  action_p =
    new QAction("Show &drift spectra window", dynamic_cast<QObject *>(this));
  action_p->setShortcut(QKeySequence("Ctrl+W, D"));
  action_p->setStatusTip("Show the drift spectra window");
  connect(
    action_p, &QAction::triggered, this, &ProgramWindow::showDriftSpectraWnd);
  mp_windowsMenu->addAction(action_p);


  action_p = new QAction("Show TIC/&XIC chrom. / mass spec. color maps window",
                         dynamic_cast<QObject *>(this));
  action_p->setShortcut(QKeySequence("Ctrl+W, I"));
  action_p->setStatusTip(
    "Show the TIC/XIC chrom. / mass spectra color maps window");
  connect(action_p,
          &QAction::triggered,
          this,
          &ProgramWindow::showTicXicChromMassSpecColorMapWnd);
  mp_windowsMenu->addAction(action_p);


  action_p = new QAction("Show TIC/XIC chrom. / drift &spec. color maps window",
                         dynamic_cast<QObject *>(this));
  action_p->setShortcut(QKeySequence("Ctrl+W, S"));
  action_p->setStatusTip(
    "Show the TIC/XIC chrom. / drift spectra color maps window");
  connect(action_p,
          &QAction::triggered,
          this,
          &ProgramWindow::showTicXicChromDriftSpecColorMapWnd);
  mp_windowsMenu->addAction(action_p);


  action_p = new QAction("Show drift s&pec. / mass spec. color maps window",
                         dynamic_cast<QObject *>(this));
  action_p->setShortcut(QKeySequence("Ctrl+W, P"));
  action_p->setStatusTip(
    "Show the drift spectra / mass spectra color maps window");
  connect(action_p,
          &QAction::triggered,
          this,
          &ProgramWindow::showDriftSpecMassSpecColorMapWnd);
  mp_windowsMenu->addAction(action_p);


  action_p =
    new QAction("Show tas&k monitor window", dynamic_cast<QObject *>(this));
  action_p->setShortcut(QKeySequence("Ctrl+W, T"));
  action_p->setStatusTip("Show the task monitor window");
  connect(
    action_p, &QAction::triggered, this, &ProgramWindow::showTaskMonitorWnd);
  mp_windowsMenu->addAction(action_p);


  action_p = new QAction("Show open ms r&un data sets window",
                         dynamic_cast<QObject *>(this));
  action_p->setShortcut(QKeySequence("Ctrl+W, O"));
  action_p->setStatusTip("Show the open MS run data sets window");
  connect(action_p,
          &QAction::triggered,
          this,
          &ProgramWindow::showOpenMsRunDataSetsWnd);

  mp_windowsMenu->addAction(action_p);


  action_p = new QAction("Show &console window", dynamic_cast<QObject *>(this));
  action_p->setShortcut(QKeySequence("Ctrl+W, C"));
  action_p->setStatusTip("Show the console window");
  connect(action_p, &QAction::triggered, this, &ProgramWindow::showConsoleWnd);

  mp_windowsMenu->addAction(action_p);

  mp_windowsMenu->addSeparator();

  action_p = new QAction("Save &workspace", dynamic_cast<QObject *>(this));
  action_p->setStatusTip("Save the current layout of the workspace");
  connect(action_p, &QAction::triggered, this, &ProgramWindow::saveWorkspace);

  mp_windowsMenu->addAction(action_p);


  // Utilities menu
  mp_utilitiesMenu = menuBar()->addMenu("&Utilities");

  action_p =
    new QAction("XIC chromatogram &extractions", dynamic_cast<QObject *>(this));
  action_p->setStatusTip("Open the XIC chromatogram extraction functionality");
  action_p->setShortcut(tr("Ctrl+X, E"));

  connect(
    action_p, &QAction::triggered, this, &ProgramWindow::showXicExtractionWnd);

  mp_utilitiesMenu->addAction(action_p);

  action_p = new QAction("&Isotopic cluster calculations",
                         dynamic_cast<QObject *>(this));
  action_p->setStatusTip("Open the isotopic cluster calculation functionality");
  action_p->setShortcut(QKeySequence("Ctrl+I, C"));

  connect(action_p,
          &QAction::triggered,
          this,
          &ProgramWindow::showIsotopicClusterGeneratorDlg);

  mp_utilitiesMenu->addAction(action_p);

  action_p =
    new QAction("Mass peak &shape calculations", dynamic_cast<QObject *>(this));
  action_p->setStatusTip("Open the mass peak shape calculation functionality");
  action_p->setShortcut(QKeySequence("Ctrl+M, S"));

  connect(action_p,
          &QAction::triggered,
          this,
          &ProgramWindow::showIsotopicClusterShaperDlg);

  mp_utilitiesMenu->addAction(action_p);


  // Menu that pops up a dialog to start the server and also to configure the
  // client to connect to a server (id address : port number).

  mp_clientServerConfigDlgAct =
    new QAction(QIcon(":/images/new.png"), "Client-Server configuration", this);
  mp_clientServerConfigDlgAct->setStatusTip(
    "Open the client-server configuration dialog window.");

  connect(mp_clientServerConfigDlgAct, &QAction::triggered, this, [this]() {
    if(mp_clientServerConfigDlg == nullptr)
      {

        mp_clientServerConfigDlg =
          new libXpertMassGui::MassDataClientServerConfigDlg(
            this, m_applicationName, "Client-server configuration");
        // At this point, make the connections with the signals provided by
        // the configuration dialog window.
        connect(mp_clientServerConfigDlg,
                &libXpertMassGui::MassDataClientServerConfigDlg::startServerSignal,
                this,
                &ProgramWindow::startServer);
        connect(mp_clientServerConfigDlg,
                &libXpertMassGui::MassDataClientServerConfigDlg::stopServerSignal,
                this,
                &ProgramWindow::stopServer);
        connect(mp_clientServerConfigDlg,
                &libXpertMassGui::MassDataClientServerConfigDlg::startClientSignal,
                this,
                &ProgramWindow::startClient);
        connect(mp_clientServerConfigDlg,
                &libXpertMassGui::MassDataClientServerConfigDlg::stopClientSignal,
                this,
                &ProgramWindow::stopClient);
      }

    if(mp_clientServerConfigDlg == nullptr)
      qFatal(
        "Programming error. Cannot be that the dialog window pointer is "
        "nullptr.");
    mp_clientServerConfigDlg->activateWindow();
    mp_clientServerConfigDlg->raise();
    mp_clientServerConfigDlg->show();
  });

  mp_utilitiesMenu->addAction(mp_clientServerConfigDlgAct);


  // Preferences menu
  mp_preferencesMenu = menuBar()->addMenu("&Preferences");

  action_p =
    new QAction("Set max. thread count", dynamic_cast<QObject *>(this));
  action_p->setStatusTip(
    "Set the maximum number of threads to be used for the integrations");
  action_p->setShortcut(tr("Ctrl+P, T"));

  connect(action_p, &QAction::triggered, [this]() {
    std::size_t ideal_thread_count = QThread::idealThreadCount();
    m_maxThreadUseCount            = QInputDialog::getInt(this,
                                               "Set max. thread count",
                                               "Count",
                                               ideal_thread_count,
                                               1,
                                               ideal_thread_count,
                                               1);
    // qDebug() << "Set the max. thread use count to : " <<
    // m_maxThreadUseCount;
  });

  mp_preferencesMenu->addAction(action_p);


  // Help menu
  mp_helpMenu = menuBar()->addMenu("&Help");

  action_p = new QAction(QIcon(":/images/svg/help-information-icon.svg"),
                         tr("&About"),
                         dynamic_cast<QObject *>(this));
  action_p->setStatusTip(tr("Show the &application's About box"));
  action_p->setShortcut(QKeySequence("Ctrl+H, A"));

  connect(action_p, &QAction::triggered, this, &ProgramWindow::showAboutDlg);
  mp_helpMenu->addAction(action_p);

  action_p = new QAction(QIcon(":/images/svg/help-qt-information-icon.svg"),
                         tr("About &Qt"),
                         dynamic_cast<QObject *>(this));
  action_p->setStatusTip(tr("Show the Qt &library's About box"));
  connect(action_p, &QAction::triggered, this, &Application::aboutQt);
  action_p->setShortcut(QKeySequence("Ctrl+H, Q"));

  mp_helpMenu->addAction(action_p);
}


void
ProgramWindow::setupWindow()
{
  // qDebug();

  QPixmap pixmap(":/images/icons/32x32/minexpert2.png");
  QIcon icon(pixmap);
  setWindowIcon(icon);

  // This attribute make sure that the main window of the program is destroyed
  // when it is closed. Effectively stopping the program.
  setAttribute(Qt::WA_DeleteOnClose);

  statusBar()->setSizeGripEnabled(true);

  createMenusAndActions();
  initializeAllWindows();
  readSettings();
}


int
ProgramWindow::selectMsRun(std::vector<pappso::MsRunIdCstSPtr> &ms_run_ids)
{
  MsRunSelectorDlg *dlg =
    new MsRunSelectorDlg(this, ms_run_ids, m_applicationName);

  return dlg->exec();
}


AboutDlg *
ProgramWindow::showAboutDlg()
{
  // The application name will be set automatically by default parameter
  // value.
  AboutDlg *dlg = new AboutDlg(this, m_applicationName);

  dlg->show();

  return dlg;
}


void
ProgramWindow::showOpenMsRunDataSetsWnd()
{
  mp_openMsRunDataSetsDlg->activateWindow();
  mp_openMsRunDataSetsDlg->raise();
  mp_openMsRunDataSetsDlg->show();
}


const OpenMsRunDataSetsDlg *
ProgramWindow::getOpenMsRunDataSetsDlg() const
{
  return mp_openMsRunDataSetsDlg;
}


void
ProgramWindow::showTicXicChromatogramsWnd()
{
  mp_ticXicChromPlotWnd->showWindow();
}


void
ProgramWindow::showMassSpectraWnd()
{
  mp_massSpecPlotWnd->showWindow();
}


void
ProgramWindow::showDriftSpectraWnd()
{
  mp_driftSpecPlotWnd->showWindow();
}


void
ProgramWindow::showTicXicChromMassSpecColorMapWnd()
{
  mp_ticXicChromMassSpecColorMapWnd->showWindow();
}


void
ProgramWindow::showTicXicChromDriftSpecColorMapWnd()
{
  mp_ticXicChromDriftSpecColorMapWnd->showWindow();
}


void
ProgramWindow::showDriftSpecMassSpecColorMapWnd()
{
  mp_driftSpecMassSpecColorMapWnd->showWindow();
}


void
ProgramWindow::showXicExtractionWnd()
{

  mp_xicExtractionWnd->showWindow();
}


void
ProgramWindow::showIsotopicClusterGeneratorDlg()
{
  mp_isotopicClusterGeneratorDlg = new libXpertMassGui::IsotopicClusterGeneratorDlg(
    this, m_applicationName, "Isotopic cluster generator");

  connect(mp_isotopicClusterGeneratorDlg,
          &libXpertMassGui::IsotopicClusterGeneratorDlg::displayMassSpectrumSignal,
          [this](const QString &title,
                 const QByteArray &color_byte_array,
                 pappso::TraceCstSPtr trace) {
            mp_massSpecPlotWnd->handleReceivedData(
              title, color_byte_array, trace);
          });

  mp_isotopicClusterGeneratorDlg->activateWindow();
  mp_isotopicClusterGeneratorDlg->raise();
  mp_isotopicClusterGeneratorDlg->show();
}


void
ProgramWindow::showIsotopicClusterShaperDlg()
{
  mp_isotopicClusterShaperDlg = new libXpertMassGui::IsotopicClusterShaperDlg(
    this, m_applicationName, "Isotopic cluster shaper");

  connect(mp_isotopicClusterShaperDlg,
          &libXpertMassGui::IsotopicClusterShaperDlg::displayMassSpectrumSignal,
          [this](const QString &title,
                 const QByteArray &color_byte_array,
                 pappso::TraceCstSPtr trace) {
            mp_massSpecPlotWnd->handleReceivedData(
              title, color_byte_array, trace);
          });

  mp_isotopicClusterShaperDlg->activateWindow();
  mp_isotopicClusterShaperDlg->raise();
  mp_isotopicClusterShaperDlg->show();
}


void
ProgramWindow::showTaskMonitorWnd()
{
  mp_taskMonitorWnd->activateWindow();
  mp_taskMonitorWnd->raise();
  mp_taskMonitorWnd->show();
}


void
ProgramWindow::showConsoleWnd()
{
  mp_consoleWnd->activateWindow();
  mp_consoleWnd->raise();
  mp_consoleWnd->show();
}


void
ProgramWindow::saveWorkspace()
{
  mp_openMsRunDataSetsDlg->writeSettings();
  mp_taskMonitorWnd->writeSettings();
  mp_consoleWnd->writeSettings();

  mp_ticXicChromPlotWnd->writeSettings();
  mp_massSpecPlotWnd->writeSettings();
  mp_driftSpecPlotWnd->writeSettings();
  mp_ticXicChromMassSpecColorMapWnd->writeSettings();
  mp_ticXicChromDriftSpecColorMapWnd->writeSettings();
  mp_driftSpecMassSpecColorMapWnd->writeSettings();

  mp_xicExtractionWnd->writeSettings();
}


//! Handler of the close event signal.
void
ProgramWindow::closeEvent(QCloseEvent *event)
{
  writeSettings();
  event->accept();
}


void
ProgramWindow::showMsRunDataSetTableViewWnd(
  MsRunDataSetCstSPtr ms_run_data_set_csp, const QColor &color)
{
  // qDebug();

  // Each ms run data set has the ability to show its data in the form of a
  // tree view in a specific window. This is what we want to do here.

  // Let's see the usage count of the MsRunDataSetCstSPtr.
  // qDebug() << "Usage count:" << ms_run_data_set_csp.use_count();

  // First check if the window exists already.

  std::map<MsRunDataSetCstSPtr, MsRunDataSetTableViewWnd *>::iterator
    res_iterator = std::find_if(
      mp_msRunDataSetTableViewWndMap.begin(),
      mp_msRunDataSetTableViewWndMap.end(),
      [ms_run_data_set_csp](
        const std::pair<MsRunDataSetCstSPtr, MsRunDataSetTableViewWnd *> item) {
        return item.first == ms_run_data_set_csp;
      });

  MsRunDataSetTableViewWnd *tree_view_wnd_p = nullptr;

  if(res_iterator == mp_msRunDataSetTableViewWndMap.end())
    {
      // The tree view window for this ms run data set was never asked to be
      // shown. Thus, do the whole work.

      tree_view_wnd_p =
        new MsRunDataSetTableViewWnd(this, this, ms_run_data_set_csp, color);

      // qDebug() << "Creating new ms run data set tree view.";

      // Immediately map the ms run data set and the tree view.

      mp_msRunDataSetTableViewWndMap[ms_run_data_set_csp] = tree_view_wnd_p;
    }
  else
    {
      // qDebug() << "A map item was found;";

      tree_view_wnd_p = res_iterator->second;
    }

  tree_view_wnd_p->activateWindow();
  tree_view_wnd_p->raise();
  tree_view_wnd_p->show();
}


bool
ProgramWindow::startServer()
{
  qDebug();

  if(mpa_massDataServer != nullptr)
    delete mpa_massDataServer;

  mpa_massDataServer = new libXpertMass::MassDataServer(this);

  if(mpa_massDataServer != nullptr)
    {
      // Actually start the server.

      if(!mpa_massDataServer->listen())
        {
          qDebug() << "Failed to start the server.";
          delete mpa_massDataServer;
          return false;
        }
    }
  else
    {
      qDebug() << "Failed to allocate the server.";
      return false;
    }

  // At this point try to get to the details.

  QString ip_address = mpa_massDataServer->serverAddress().toString();
  int port_number    = mpa_massDataServer->serverPort();

  libXpertMassGui::MassDataClientServerConfigDlg *sender_p =
    static_cast<libXpertMassGui::MassDataClientServerConfigDlg *>(QObject::sender());

  sender_p->updateClientConfigurationData(ip_address, port_number);

  return true;
}


void
ProgramWindow::stopServer()
{
  if(mpa_massDataServer != nullptr)
    {
      delete mpa_massDataServer;
      mpa_massDataServer = nullptr;
    }
}


bool
ProgramWindow::startClient(const QString &ip_address,
                           int port_number,
                           int connection_frequency)
{
  qDebug();

  if(mpa_massDataClient != nullptr)
    delete mpa_massDataClient;

  mpa_massDataClient = new libXpertMass::MassDataClient(
    ip_address, port_number, connection_frequency, this);

  if(mpa_massDataClient != nullptr)
    {
      // Now connect to the error feedback signal so that the server settings
      // dialog display the errors.

      connect(mpa_massDataClient,
              &libXpertMass::MassDataClient::reportErrorSignal,
              [this](const QString &error) {
                qDebug();
                if(mp_clientServerConfigDlg)
                  mp_clientServerConfigDlg->clientFailingFeedback(error);
              });

      connect(mpa_massDataClient,
              &libXpertMass::MassDataClient::newDataSignal,
              [&](const QByteArray &byte_array) {
                qDebug() << "Received new data of size:" << byte_array.size();
                dispatchReceivedData(byte_array);
              });

      connect(mpa_massDataClient,
              &libXpertMass::MassDataClient::hostFoundSignal,
              [this]() {
                if(mp_clientServerConfigDlg)
                  mp_clientServerConfigDlg->message("The remote host was found",
                                                    6000);
              });

      connect(mpa_massDataClient,
              &libXpertMass::MassDataClient::connectedSignal,
              [this]() {
                if(mp_clientServerConfigDlg)
                  mp_clientServerConfigDlg->message(
                    "Connected to the remote host", 6000);
              });
      return true;
    }

  return false;
}


void
ProgramWindow::stopClient()
{
  if(mpa_massDataClient != nullptr)
    {
      delete mpa_massDataClient;
      mpa_massDataClient = nullptr;
    }
}


void
ProgramWindow::dispatchReceivedData(const QByteArray &byte_array)
{
  // We receive the byte_array from the TCP socket and we need to actually
  // determine what kind of data it contains. Depending on the data type, the
  // data are dispatched to the proper window.

  libXpertMass::MassDataType mass_data_type =
    libXpertMass::MassDataCborBaseHandler::readMassDataType(byte_array);

  qDebug() << "Mass data type:" << (int)mass_data_type;

  if(mass_data_type == libXpertMass::MassDataType::UNSET)
    {
      qDebug() << "The mass data type is libXpertMass::MassDataType::UNSET";
    }
  else if(mass_data_type == libXpertMass::MassDataType::MASS_SPECTRUM)
    {
      qDebug() << "The mass data type is libXpertMass::MassDataType::MASS_SPECTRUM";

      mp_massSpecPlotWnd->handleReceivedData(byte_array);
    }
  else
    {
      qDebug() << "The mass data type is not yet supported.";
    }
}


void
ProgramWindow::massDataToBeServed(const QByteArray &byte_array)
{
  qDebug() << "Got signal that data had to be served; size:"
           << byte_array.size();

  if(mpa_massDataServer != nullptr)
    mpa_massDataServer->serveData(byte_array);

  else
    QMessageBox::warning(
      this, "Mass data server", "Please, start the mass data server");
}

} // namespace minexpert

} // namespace MsXpS
