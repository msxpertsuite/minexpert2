/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2021 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */

#pragma once


/////////////////////// StdLib includes


/////////////////////// Qt includes
#include <QWidget>
#include <QPainter>
#include <QPointer>
#include <QEvent>


namespace MsXpS
{
namespace minexpert
{


class PlotAreaOverlay : public QWidget
{
  Q_OBJECT

  friend class PlotAreaOverlayFactoryFilter;


  public:
  explicit PlotAreaOverlay(QWidget *parent = nullptr);
  virtual ~PlotAreaOverlay();

  void setVertRulerVisible(bool is_visible);
  bool isVertRulerVisible();

  void setLastHorSliderValue(int value);

  virtual void paintEvent(QPaintEvent *) override;

  void updateSizeAndPosition(QSize new_size, QPoint global_new_pos);

  protected:
  bool m_isVertRulerVisible = false;

  int m_horSliderValue;
};


} // namespace minexpert

} // namespace MsXpS
