/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2019 Filippo Rusconi
 *
 * Inspiration from XTPcpp by Olivier Langella (olivier.langella@u-psud.fr)
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */

#pragma once

/////////////////////// StdLib includes


/////////////////////// Qt includes
#include <QThread>
#include <QCloseEvent>


/////////////////////// pappsomspp includes
#include <pappsomspp/msfile/msfileaccessor.h>


/////////////////////// Local includes
#include "../nongui/MsRunProber.hpp"

namespace MsXpS
{
namespace minexpert
{

class ProgramWindow;

class MsRunProbeTask : public QObject
{
  Q_OBJECT

  public:
  MsRunProbeTask(ProgramWindow *program_window_p);
  ~MsRunProbeTask();

  protected:
  void closeEvent(QCloseEvent *event);

  public slots:
  void probeMsRun(MsRunProbeTask *ms_run_probe_task_p,
                  MsRunProberSPtr ms_run_prober_sp);

  signals:
  void finishedProbingMsRunSignal(MsRunProberSPtr ms_run_prober_sp,
                                  MsRunProbeInfoSPtr ms_run_probe_info_sp,
                                  double duration_in_seconds);

  private:
  ProgramWindow *mp_programWindow;
};


typedef std::shared_ptr<MsRunProbeTask> MsRunProbeTaskSPtr;

} // namespace minexpert

} // namespace MsXpS


Q_DECLARE_METATYPE(MsXpS::minexpert::MsRunProbeTask)
extern int MsRunProbeTaskMetaTypeId;

Q_DECLARE_METATYPE(MsXpS::minexpert::MsRunProbeTaskSPtr)
extern int MsRunProbeTaskSPtrMetaTypeId;
