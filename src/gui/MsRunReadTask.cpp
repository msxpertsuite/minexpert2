/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2019 Filippo Rusconi
 *
 * Inspiration from XTPcpp by Olivier Langella (olivier.langella@u-psud.fr)
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


/////////////////////// StdLib includes
#include <iostream>
#include <chrono>

/////////////////////// Qt includes
#include <QDebug>
#include <QSettings>
#include <QThread>
#include <QMessageBox>


/////////////////////// pappsomspp includes
#include <pappsomspp/msfile/msfileaccessor.h>
#include <pappsomspp/msrun/msrunreadconfig.h>
#include <pappsomspp/exception/exceptioninterrupted.h>


/////////////////////// Local includes
#include "MsRunReadTask.hpp"
#include "../nongui/MassSpecDataFileLoader.hpp"
// #include <qapplication.h>


int MsRunReadTaskMetaTypeId =
  qRegisterMetaType<MsXpS::minexpert::MsRunReadTask>(
    "MsXpS::minexpert::MsRunReadTask");

int MsRunReadTaskSPtrMetaTypeId =
  qRegisterMetaType<MsXpS::minexpert::MsRunReadTaskSPtr>(
    "MsXpS::minexpert::MsRunReadTaskSPtr");


namespace MsXpS
{
namespace minexpert
{


MsRunReadTask::MsRunReadTask(ProgramWindow *program_window_p)
  : QObject(), mp_programWindow(program_window_p)
{
}

MsRunReadTask::MsRunReadTask(const MsRunReadTask &other)
  : QObject(), mp_programWindow(other.mp_programWindow)
{
}

MsRunReadTask::~MsRunReadTask()
{
  // qDebug() << "MsRunReadTask::MsRunReadTask destructor";
}


void
MsRunReadTask::readMsRunData(MsRunReadTask *ms_run_read_task_p,
                             pappso::MsRunReaderSPtr ms_run_reader_sp,
                             const pappso::MsRunReadConfig &ms_run_read_config,
                             MassSpecDataFileLoaderSPtr loader_sp)
{
  // We need to ensure that we react to a signal that indeed is directed to the
  // proper MsRunReadTask.

  if(this != ms_run_read_task_p)
    return;

  // qDebug() << "reading ms run data from thread:" << QThread::currentThread()
  //<< "and this MsRunReadTask:" << this;

  // qDebug("Reading MS run data with config: %s\n",
  //        ms_run_read_config.toString().toLatin1().data());

  const auto start_time = std::chrono::steady_clock::now();

  try
    {
      ms_run_reader_sp->readSpectrumCollection2(ms_run_read_config,
                                                *(loader_sp.get()));
    }
  catch(pappso::ExceptionInterrupted &e)
    {
      qDebug() << "Interruption:" << e.what();

      emit finishedReadingMsRunDataSignal(loader_sp->getMsRunDataSet(), 0);
      return;
    }

  const auto end_time = std::chrono::steady_clock::now();

  double duration_in_seconds =
    std::chrono::duration_cast<std::chrono::seconds>(end_time - start_time)
      .count();

  // qDebug() << "Finished reading spectrum collection. Number of spectra:"
  //          << loader_sp->getMsRunDataSet()->size()
  //          << "-- time used (seconds): " << duration_in_seconds;

  //  To debug the fact that the signal is effectively emitted,  which is true
  //  since the aboutQt dialog shows up.
  // connect(this, SIGNAL(finishedReadingMsRunDataSignal(MsRunDataSetSPtr,
  // double)), qApp, SLOT(aboutQt()));

  // At this point, emit the signal that we did the job.
  emit finishedReadingMsRunDataSignal(loader_sp->getMsRunDataSet(),
                                      duration_in_seconds);

  // qDebug() << "Emitted the finishedReadingMsRunDataSignal signal.";
}


} // namespace minexpert

} // namespace MsXpS
