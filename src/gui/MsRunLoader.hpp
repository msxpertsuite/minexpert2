/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2019 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#pragma once


/////////////////////// StdLib includes


/////////////////////// Qt includes
#include <QMainWindow>
#include <QDir>


/////////////////////// pappsomspp includes
#include <pappsomspp/msfile/msfileaccessor.h>


/////////////////////// Local includes
#include "config.h"


namespace MsXpS
{
namespace minexpert
{

class ProgramWindow;

class MsRunLoader : public QObject
{
  Q_OBJECT

  public:
  explicit MsRunLoader(ProgramWindow *program_window_p,
                       pappso::MsFileAccessorSPtr ms_file_accessor_sp);
  virtual ~MsRunLoader();

  // This is the main interface. In derived classes, loadMsRun() will call
  // helper functions in cases where the loading cannot occur easily right away.
  // For example, when loading Bruker timsTOF data, there is a probing function
  // that is called that will present the user with metadata to help them
  // configure the way data are actually loaded. Conversely, with mzML data (pwiz), the loadMsRun does the job right away.
  virtual void loadMsRun(bool full_in_memory = false) = 0;

  protected:
  ProgramWindow *mp_programWindow;
  pappso::MsFileAccessorSPtr msp_msFileAccessor;
};

typedef std::shared_ptr<MsRunLoader> MsRunLoaderSPtr;

} // namespace minexpert

} // namespace MsXpS


Q_DECLARE_METATYPE(MsXpS::minexpert::MsRunLoader)
extern int MsRunLoaderMetaTypeId;

Q_DECLARE_METATYPE(MsXpS::minexpert::MsRunLoaderSPtr)
extern int MsRunLoaderSPtrMetaTypeId;
