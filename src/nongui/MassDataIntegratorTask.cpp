/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2019 Filippo Rusconi
 *
 * Inspiration from XTPcpp by Olivier Langella (olivier.langella@u-psud.fr)
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


/////////////////////// StdLib includes
#include <iostream>


/////////////////////// Qt includes
#include <QDebug>
#include <QSettings>
#include <QThread>
#include <QMessageBox>


/////////////////////// pappsomspp includes
#include <pappsomspp/msfile/msfileaccessor.h>


/////////////////////// Local includes
#include "MassDataIntegratorTask.hpp"


namespace MsXpS
{
namespace minexpert
{


MassDataIntegratorTask::MassDataIntegratorTask()
{
  // qDebug();
}


MassDataIntegratorTask::~MassDataIntegratorTask()
{
  // qDebug() << "MassDataIntegratorTask::MassDataIntegratorTask destructor";
}


void
MassDataIntegratorTask::seedInitialTicChromatogramAndMsRunDataSetStatistics(
  MsRunDataSetTreeMassDataIntegratorToRt *mass_data_integrator_p)
{
  mass_data_integrator_p->seedInitialTicChromatogramAndMsRunDataSetStatistics();

  emit finishedIntegratingDataSignal(mass_data_integrator_p);
}


void
MassDataIntegratorTask::integrateToRt(
  MsRunDataSetTreeMassDataIntegratorToRt *mass_data_integrator_p)
{
  // qDebug() << "integrating data to a xic chromatogram from thread:"
  //<< QThread::currentThread()
  //<< "with the integrator pointer:" << mass_data_integrator_p;

  mass_data_integrator_p->integrateToRt();

  // At this point, emit the signal that we did the job.
  emit finishedIntegratingDataSignal(mass_data_integrator_p);
}


void
MassDataIntegratorTask::integrateToRt(
  QualifiedMassSpectrumVectorMassDataIntegratorToRt *mass_data_integrator_p)
{
  mass_data_integrator_p->integrate();

  // At this point, emit the signal that we did the job.

  // qDebug()
  //<< "Going to emit finishedIntegratingDataSignal(mass_data_integrator_p);";

  emit finishedIntegratingDataSignal(mass_data_integrator_p);
}


void
MassDataIntegratorTask::integrateToMz(
  QualifiedMassSpectrumVectorMassDataIntegratorToMz *mass_data_integrator_p)
{
  // qDebug() << "integrating data to a mass spectrum from thread:"
  //<< QThread::currentThread()
  //<< "with the integrator pointer:" << mass_data_integrator_p;

  mass_data_integrator_p->integrate();

  // At this point, emit the signal that we did the job.

  // qDebug()
  //<< "Going to emit finishedIntegratingDataSignal(mass_data_integrator_p);";

  emit finishedIntegratingDataSignal(mass_data_integrator_p);
}


void
MassDataIntegratorTask::integrateToDt(
  QualifiedMassSpectrumVectorMassDataIntegratorToDt *mass_data_integrator_p)
{
  mass_data_integrator_p->integrate();

  // At this point, emit the signal that we did the job.

  // qDebug()
  //<< "Going to emit finishedIntegratingDataSignal(mass_data_integrator_p);";

  emit finishedIntegratingDataSignal(mass_data_integrator_p);
}


void
MassDataIntegratorTask::integrateToTicIntensity(
  QualifiedMassSpectrumVectorMassDataIntegratorToTicInt *mass_data_integrator_p)
{
  mass_data_integrator_p->integrate();

  // At this point, emit the signal that we did the job.

  // qDebug()
  //<< "Going to emit finishedIntegratingDataSignal(mass_data_integrator_p);";

  emit finishedIntegratingDataSignal(mass_data_integrator_p);
}


void
MassDataIntegratorTask::integrateToDtRtMz(
  QualifiedMassSpectrumVectorMassDataIntegratorToDtRtMz *mass_data_integrator_p)
{
  // qDebug() << "Data kind:" << static_cast<int>(data_kind);

  mass_data_integrator_p->integrate();

  // At this point, emit the signal that we did the job.

  // qDebug()
  //<< "Going to emit finishedIntegratingDataSignal(mass_data_integrator_p);";

  emit finishedIntegratingDataSignal(mass_data_integrator_p);
}


void
MassDataIntegratorTask::integrateToDtRt(
  QualifiedMassSpectrumVectorMassDataIntegratorToRtDt *mass_data_integrator_p)
{
  mass_data_integrator_p->integrate();

  // qDebug() << "integrating with kind:" << static_cast<int>(data_kind);

  // At this point, emit the signal that we did the job.

  // qDebug()
  //<< "Going to emit finishedIntegratingDataSignal(mass_data_integrator_p);";

  emit finishedIntegratingDataSignal(mass_data_integrator_p);
}


} // namespace minexpert

} // namespace MsXpS
