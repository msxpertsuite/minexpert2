/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2019 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#pragma once

/////////////////////// StdLib includes
#include <map>


/////////////////////// Qt includes
#include <QString>
#include <QDateTime>


/////////////////////// pappsomspp includes
#include <pappsomspp/processing/filters/filterinterface.h>
#include <pappsomspp/processing/filters/filtersuitestring.h>


/////////////////////// Local includes
#include "MsRunDataSet.hpp"
#include "MsFragmentationSpec.hpp"
#include "ProcessingStep.hpp"

namespace MsXpS
{
namespace minexpert
{

class ProcessingType;
class ProcessingStep;

class ProcessingFlow : public std::vector<ProcessingStepCstSPtr>
{

  public:
  ProcessingFlow();
  ProcessingFlow(MsRunDataSetCstSPtr ms_run_data_set_csp);
  ProcessingFlow(const ProcessingFlow &other);

  virtual ~ProcessingFlow();

  ProcessingFlow &operator=(const ProcessingFlow &other);

  std::vector<ProcessingStepCstSPtr> steps();

  const std::map<QString, pappso::FilterNameInterfaceCstSPtr> &
  getFilterMap() const;

  void addFilter(pappso::FilterNameInterfaceCstSPtr filter_csp);
  std::size_t removeFilter(const QString &filter_name);

  pappso::FilterNameInterfaceCstSPtr filter(const QString &filter_name) const;

  bool
  innermostRange(ProcessingType src_processing_type_mask,
                 std::pair<double, double> &out_range,
                 std::vector<ProcessingStepCstSPtr> *out_processing_steps_p,
                 bool store_only_2d_steps = true,
                 bool store_only_skewed   = true) const;

  bool innermostRange(
    const QString src_processing_type_mask,
    std::pair<double, double> &out_range,
    std::vector<ProcessingStepCstSPtr> *out_processing_steps_p = nullptr,
    bool store_only_2d_steps                                   = true,
    bool store_only_rhomboid                                   = true) const;

  size_t greatestMsLevel() const;

  ProcessingStepCstSPtr oldestStep() const;
  ProcessingStepCstSPtr closestOlderStep(const QDateTime &date_time) const;
  ProcessingStepCstSPtr mostRecentStep() const;

  std::vector<ProcessingStepCstSPtr>
  stepsMatchingDestType(const ProcessingType &processing_type,
                        bool with_valid_fragmentation_spec = false) const;

  std::vector<ProcessingStepCstSPtr>
  stepsMatchingDestType(const QString &processing_type,
                        bool with_valid_fragmentation_spec = false) const;

  void setDefaultMzIntegrationParams(
    const pappso::MzIntegrationParams &mz_integration_params);
  const pappso::MzIntegrationParams &getDefaultMzIntegrationParams() const;
  const pappso::MzIntegrationParams *mostRecentMzIntegrationParams() const;

  void setDefaultMsFragmentationSpec(
    const MsFragmentationSpec &ms_fragmentation_spec);
  const MsFragmentationSpec &getDefaultMsFragmentationSpec() const;


  void setMsRunDataSetCstSPtr(MsRunDataSetCstSPtr ms_run_data_set_csp);
  MsRunDataSetCstSPtr getMsRunDataSetCstSPtr() const;

  QString toString(int offset = 0, const QString &spacer = QString()) const;

  protected:
  MsRunDataSetCstSPtr mcsp_msRunDataSet = nullptr;

  // Note that each step has an optional pappso::MzIntegrationParams *. This one
  // is the default one when not a single step has mz integration params. This
  // one is set automatically in the first graph obtained when displaying the
  // TIC chromatogram right after having loaded the ms run from file.
  pappso::MzIntegrationParams m_defaultMzIntegrationParams;

  // Same for MsFragmentationSpec
  MsFragmentationSpec m_defaultMsFragmentationSpec;

  // A map providing mapping of filter name and filter to be applied to the
  // results of the processing. Mind that this is a map, not a multimap.
  std::map<QString, pappso::FilterNameInterfaceCstSPtr> m_filterMap;
};


} // namespace minexpert

} // namespace MsXpS
