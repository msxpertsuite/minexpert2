/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright (C) 2009--2020 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


/////////////////////// Qt includes
#include <QObject>
#include <QMainWindow>
#include <QHash>
#include <QMap>
#include <QDebug>
#include <QThread>


/////////////////////// Local includes
#include "TicChromatogramFromMassSpecDataFile.hpp"


int ticChromatogramFromMassSpecDataFileSPtrMetaTypeId =
  qRegisterMetaType<MsXpS::minexpert::TicChromatogramFromMassSpecDataFileSPtr>(
    "MsXpS::minexpert::TicChromatogramFromMassSpecDataFileSPtr");


namespace MsXpS
{
namespace minexpert
{


//! Construct a TicChromatogramFromMassSpecDataFile instance.
/*!

  \param Name of the file to load.

*/
TicChromatogramFromMassSpecDataFile::TicChromatogramFromMassSpecDataFile(
  const QString &fileName, bool load_fully_in_memory)
  : m_isLoadFullInMemory(load_fully_in_memory)
{
  if(!fileName.isEmpty())
    m_fileName = fileName;
}


//! Destruct \c this TicChromatogramFromMassSpecDataFile instance.
TicChromatogramFromMassSpecDataFile::~TicChromatogramFromMassSpecDataFile()
{
}


//! Set the MsRunDataSet in which to store the mass data.
/*!

  When loading a mass spectrometry data file, the data are stored in various
  structures all belonging to the MsRunDataSet instance passed as
  parameter.

  \param msRunDataSet pointer to a MsRunDataSet instance in which to
  store the mass data loaded from file.

*/
void
TicChromatogramFromMassSpecDataFile::setMsRunDataSet(
  MsRunDataSetSPtr &ms_run_data_set_sp)
{
  if(ms_run_data_set_sp == nullptr)
    qFatal("Programming error.");

  // Let's see the usage count of the MsRunDataSetCstSPtr.
  // qDebug() << "Usage count:" << ms_run_data_set_sp.use_count();

  msp_msRunDataSet = ms_run_data_set_sp;

  // Let's see the usage count of the MsRunDataSetCstSPtr.
  // qDebug() << "Usage count:" << ms_run_data_set_sp.use_count();
}


MsRunDataSetSPtr
TicChromatogramFromMassSpecDataFile::getMsRunDataSet()
{
  return msp_msRunDataSet;
}


//! Set the name of the file to load.
/*!

  \param fileName name of the file.

*/
void
TicChromatogramFromMassSpecDataFile::setFileName(const QString &fileName)
{
  m_fileName = fileName;
}


//! Cancel the running operation.
/*!

  This function can be invoked to ask the loader to stop loading mass data from
  the file.

*/
void
TicChromatogramFromMassSpecDataFile::cancelOperation()
{
  m_isOperationCancelled = true;
}


bool
TicChromatogramFromMassSpecDataFile::shouldStop()
{
  return m_isOperationCancelled;
}


void
TicChromatogramFromMassSpecDataFile::spectrumListHasSize(std::size_t size)
{
  m_spectrumListSize = size;

  emit spectrumListSizeSignal(size);

  // qDebug() << "The spectrum list has size:" << m_spectrumListSize;
}


// This function is a callback that is called each time a new spectrum is read
// from file. The data reading work is performed by pappso::MsRunReaderSp. Note
// that the qualified mass spectrum might have a mass spectrum pointer that is
// nullptr if the binary data (mz,i) were not asked for.
void
TicChromatogramFromMassSpecDataFile::setQualifiedMassSpectrum(
  const pappso::QualifiedMassSpectrum &qualified_mass_spectrum)
{
  // qDebug() << "from thread:" << QThread::currentThread()
  //<< "qualified mass spectrum:" << &qualified_mass_spectrum;

  if(qualified_mass_spectrum.isEmptyMassSpectrum())
    {
      // qDebug() << "Spectrum has no binary data, returning.";
      return;
    }

  // Let the base class do its work.
  pappso::MsRunReaderTicChromatogram::setQualifiedMassSpectrum(
    qualified_mass_spectrum);

  // Increment the count of qualified mass spectra that were processed and emit
  // that new count.  And emit a textual representation of the current task
  // situation.

  ++m_setQualifiedMassSpectrumCount;

  emit setStatusTextAndCurrentValueSignal(
    QString("Accounted for %1 spectra over %2")
      .arg(m_setQualifiedMassSpectrumCount)
      .arg(m_spectrumListSize),
    m_setQualifiedMassSpectrumCount);
}


bool
TicChromatogramFromMassSpecDataFile::needPeakList() const
{
  return m_isLoadFullInMemory;
}


void
TicChromatogramFromMassSpecDataFile::loadingEnded()
{
  // qDebug() << "Loading ended !";
}

} // namespace minexpert
} // namespace MsXpS
