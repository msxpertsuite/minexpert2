/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright (C) 2009--2020 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 * * This file is part of the msXpertSuite project.  * * The msXpertSuite
 * project is the successor of the massXpert project. This project now includes
 * various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


/////////////////////// StdLib includes
#include <vector>
#include <cmath>


/////////////////////// Qt includes
#include <QDebug>


/////////////////////// pappsomspp includes


/////////////////////// Local includes
#include "MsRunProber.hpp"


int MsRunProberMetaTypeId = qRegisterMetaType<MsXpS::minexpert::MsRunProber>(
  "MsXpS::minexpert::MsRunProber");

int MsRunProberSPtrMetaTypeId =
  qRegisterMetaType<MsXpS::minexpert::MsRunProberSPtr>(
    "MsXpS::minexpert::MsRunProberSPtr");


namespace MsXpS
{
namespace minexpert
{

// Needed for qRegisterMetaType
MsRunProber::MsRunProber(pappso::MsFileAccessorSPtr ms_file_accessor_sp)
  : QObject(), msp_msFileAccessor(ms_file_accessor_sp)
{
}


MsRunProber::~MsRunProber()
{
}

pappso::MsFileAccessorSPtr
MsRunProber::getMsFileAccessorSPtr()
{
  return msp_msFileAccessor;
}


void
MsRunProber::setReadMsRunFullyInMemory(bool is_full_in_memory)
{
  m_isReadMsRunFullyInMemory = is_full_in_memory;
}


bool
MsRunProber::isReadMsRunFullyInMemory() const
{
  return m_isReadMsRunFullyInMemory;
}


bool
MsRunProber::shouldStop() const
{
  return m_isOperationCancelled;
}

void
MsRunProber::cancelOperation()
{
  m_isOperationCancelled = true;

  qDebug() << "Cancellation asked, setting m_isOperationCancelled "
              "to true and emitting cancelOperationSignal ";

  emit cancelOperationSignal();
}


} // namespace minexpert

} // namespace MsXpS
