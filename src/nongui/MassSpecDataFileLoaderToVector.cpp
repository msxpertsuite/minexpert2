/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright (C) 2009--2024 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 * * This file is part of the msXpertSuite project.  * * The msXpertSuite
 * project is the successor of the massXpert project. This project now includes
 * various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


/////////////////////// Qt includes
#include <QDebug>
#include <QObject>

/////////////////////// pappsomspp includes
#include <pappsomspp/exception/exceptionnotfound.h>
#include <pappsomspp/msrun/spectrumcollectionhandlerinterface.h>


/////////////////////// Local includes
#include "MassSpecDataFileLoaderToVector.hpp"


namespace MsXpS
{
namespace minexpert
{


MassSpecDataFileLoaderToVector::MassSpecDataFileLoaderToVector(
  bool load_fully_in_memory)
  : m_isLoadFullInMemory(load_fully_in_memory)
{
}

MassSpecDataFileLoaderToVector::~MassSpecDataFileLoaderToVector()
{
}

bool
MassSpecDataFileLoaderToVector::needPeakList() const
{
  return m_isLoadFullInMemory;
}

void
MassSpecDataFileLoaderToVector::spectrumListHasSize(std::size_t size)
{
  m_spectrumListSize = size;

  emit spectrumListSizeSignal(size);

  // qDebug() << "The spectrum list has size:" << m_spectrumListSize;
}

void
MassSpecDataFileLoaderToVector::setQualifiedMassSpectrum(
  const pappso::QualifiedMassSpectrum &qualified_mass_spectrum)
{
  // qDebug() << "Setting qualified mass spectrum:"
  //          << qualified_mass_spectrum.getMassSpectrumId().getNativeId();

  // We know that the mass spectrum can be nullptr, because this
  // read mass spectrum collection handler might work with the
  // needPeakList set to false (not reading mass spectra from file, thus
  // not having mass spectral data, thus having mass spectrum nullptr.)
  // if(qualified_mass_spectrum.getMassSpectrumCstSPtr() == nullptr)
    // qFatal("The mass spectrum is nullptr.");

  m_qualifiedMassSpectra.push_back(qualified_mass_spectrum);
}

const std::vector<pappso::QualifiedMassSpectrum> &
MassSpecDataFileLoaderToVector::getQualifiedMassSpectra() const
{
  return m_qualifiedMassSpectra;
}

void
MassSpecDataFileLoaderToVector::clearMassSpectra()
{
  m_qualifiedMassSpectra.clear();
}

void
MassSpecDataFileLoaderToVector::cancelOperation()
{
  qDebug() << "Cancelled operation.";
  m_isOperationCancelled = true;
}

bool
MassSpecDataFileLoaderToVector::shouldStop()
{
  return m_isOperationCancelled;
}

void
MassSpecDataFileLoaderToVector::loadingEnded()
{
  // qDebug() << "Loading ended !";
}

} // namespace minexpert

} // namespace MsXpS
