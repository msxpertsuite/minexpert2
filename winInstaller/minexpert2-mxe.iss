[Setup]
AppName=mineXpert2

; Set version number below
#define public version "9.5.0"
AppVersion={#version}

#define public arch "mingw64"
#define public platform "win10"
#define public buildsystem "MXE"

;;;;;; On Wine, z: drive is configured to point to /home/rusconi
;;;;;; Check the winecfg window's Drives tab

#define sourceDir "z:/devel/minexpert2/development"

#define buildDir "z:/devel/minexpert2/build-area/mxe"

#define docBuildDir "z:/devel/minexpert2/build-area/unix/doc/user-manual"

#define mxeDllDir "z:/devel/mxe/dlls-and-stuff-for-packages"


; Set version number below
AppVerName=mineXpert2 version {#version}
DefaultDirName={commonpf}\mineXpert2
DefaultGroupName=mineXpert2
OutputDir="{#sourceDir}\winInstaller"

OutputBaseFilename=mineXpert2-{#arch}-{#platform}-{#buildsystem}-v{#version}-setup

OutputManifestFile=mineXpert2-{#arch}-{#platform}-v{#version}-setup-manifest.txt
ArchitecturesAllowed=x64
ArchitecturesInstallIn64BitMode=x64

LicenseFile="{#sourceDir}/LICENSE"
AppCopyright="Copyright (C) 2009-2023 Filippo Rusconi"

AllowNoIcons=yes
AlwaysShowComponentsList=yes
AllowRootDirectory=no
AllowCancelDuringInstall=yes
AppComments=mineXpert2, by Filippo Rusconi"
AppContact="Filippo Rusconi, PhD, Research scientist at CNRS, France"
CloseApplications=yes
CreateUninstallRegKey=yes
DirExistsWarning=yes
WindowResizable=yes
WizardImageFile="{#sourceDir}\images\splashscreen-minexpert2-innosetup.bmp"
WizardImageStretch=yes

[Dirs]
Name: "{app}\data"
Name: "{app}\doc"

[Files]
Source: "{#mxeDllDir}/*"; DestDir: {app}; Flags: ignoreversion recursesubdirs;

Source: "{#sourceDir}/doc/history.html"; DestDir: {app}\doc;

Source: "{#buildDir}/src/mineXpert2.exe"; DestDir: {app};

Source: "{#docBuildDir}/minexpert2-doc.pdf"; DestDir: {app}\doc;
Source: "{#docBuildDir}/html/user-manual/*"; Flags: recursesubdirs; DestDir: {app}\doc\html;

[Icons]
Name: "{group}\mineXpert2"; Filename: "{app}\mineXpert2.exe"; WorkingDir: "{app}"
Name: "{group}\Uninstall mineXpert2"; Filename: "{uninstallexe}"

[Run]
Filename: "{app}\mineXpert2.exe"; Description: "Launch mineXpert2"; Flags: postinstall nowait unchecked

