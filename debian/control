Source: minexpert2
Section: science
Priority: optional
Maintainer: The Debichem Group <debichem-devel@lists.alioth.debian.org>
Uploaders: Filippo Rusconi <lopippo@debian.org>
Build-Depends: debhelper-compat (= 13),
 dpkg-dev (>= 1.21.22),
# To export from svg to png
 inkscape,
 libgomp1,
 qt6-base-dev,
 libqt6svg6-dev,
 libqt6core5compat6-dev,
 libxkbcommon-x11-dev,
 cmake,
 daps,
 libjs-jquery,
 libjs-highlight.js,
 texlive-fonts-extra,
 fonts-ebgaramond-extra,
 fonts-urw-base35,
 libqcustomplot-dev,
 libisospec++-dev,
 libquazip1-qt6-dev,
 zlib1g-dev,
 libpappsomspp-dev (>= 0.9.38),
 libpappsomspp-widget-dev (>= 0.9.34),
 libxpertmass-dev (>= 1.0.0),
 libxpertmassgui-dev (>= 1.0.0),
 docbook-xml,
 docbook-to-man
Standards-Version: 4.6.2
Homepage: http://www.msxpertsuite.org/
Vcs-Browser: https://salsa.debian.org/debichem-team/minexpert2
Vcs-Git: https://salsa.debian.org/debichem-team/minexpert2.git
Rules-Requires-Root: no

Package: minexpert2
Architecture: any
Depends: ${shlibs:Depends},
 ${misc:Depends}
Suggests: minexpert2-doc (>= ${source:Upstream-Version})
Description: MS^n mass spectrometric data visualization and mining (runtime)
 mineXpert2 allows the user to perform the following tasks:
  - Open mass spectrometry data files (mzML, mzXML, asc, xy, ...);
  - Display in a table view the full data set, for easy filtering;
  - Calculate and display the TIC chromatogram;
  - Calculate and display a mz=f(rt) color map;
  - For mobility data, calculate and display a mz=f(dt) color map;
  - Integrate the mass spectrometric data from any kind of data
    representation (mass | drift spectra, TIC | XIC chromatogram,
    2D color maps) to any other kind of data representation;
  - For any mass data feature (mass peak, TIC | XIC peak, color map)
    integrate to a XIC single intensity value;
  - Powerful isotopic cluster calculation starting from a
    chemical formula, optionally with user-defined isotopic abundance ratios;
  - Gaussian fit over any isotopic cluster to estimate the
    average mass of a given ion;
  - Mouse-driven deconvolution of m/z data (charge envelope-based
    or isotopic cluster-based);
  - Export the data to text files;
  - Export the graphical representation of mass spectrometric
    data to graphics files in a number of formats (jpg, png, pdf).
 .
 This package ships the mineXpert2 program.


Package: minexpert2-doc
Section: doc
Architecture: all
Depends: libjs-jquery,
         libjs-highlight.js,
         ${misc:Depends}
Description: MS^n mass spectrometric data visualization and mining (doc)
 mineXpert2 allows the user to perform the following tasks:
  - Open mass spectrometry data files (mzML, mzXML, asc, xy, ...);
  - Display in a table view the full data set, for easy filtering;
  - Calculate and display the TIC chromatogram;
  - Calculate and display a mz=f(rt) color map;
  - For mobility data, calculate and display a mz=f(dt) color map;
  - Integrate the mass spectrometric data from any kind of data
    representation (mass | drift spectra, TIC | XIC chromatogram,
    2D color maps) to any other kind of data representation;
  - For any mass data feature (mass peak, TIC | XIC peak, color map)
    integrate to a XIC single intensity value;
  - Powerful isotopic cluster calculation starting from a
    chemical formula, optionally with user-defined isotopic abundance ratios;
  - Gaussian fit over any isotopic cluster to estimate the
    average mass of a given ion;
  - Mouse-driven deconvolution of m/z data (charge envelope-based
    or isotopic cluster-based);
  - Export the data to text files;
  - Export the graphical representation of mass spectrometric
    data to graphics files in a number of formats (jpg, png, pdf).
 .
 This package ships the user manual in both PDF and HTML formats.

