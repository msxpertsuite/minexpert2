mineXpert2
==========

mineXpert2 (of the http://www.msxpertsuite.org software collection) is a program
that allows one to perform the following tasks:

 - Open mass spectrometry data files (mzML, mzXML, asc, xy, ...);
 - Full-depth MS^n data exploration;
 - Perform mouse-based deconvolution of isotopic clusters or of charge state envelopes;
 - Calculate and display the TIC chromatogram and various color maps (see below);
 - For mobility data, calculate and display a mz=f(dt) color map;
 - Gather all mass data into a single table view for easy filtering of the data
   according to a number of criteria; then, select the desirable data rows of 
   the table view and perfom any kind of integration;
 - Integrate the data from any data type to any other data type:
    - From TIC to:
      - to mass spectrum;
      - to drift spectrum;
      - to various color maps:
        - m/z vs drift time;
        - m/z vs retention time;
        - retention time vs drift time.
    - From mass specturm to any other as above;
    - From any to any.
 - Perform single-point integrations for detailed mass spectral data scrutiny;
 - A special integration mode does not produce any trace, spectrum or color map, 
   it will produce an intensity integration on the relevant data subset and yield
   an intensity value. This hyper-specific feature is designed to perform the most
   accurate mass data quantification comparisons between data sets.
 - Export the data to text files;
 - Save plot to graphics files (svg, pdf, png, jpg);
 - Perform isotopic cluster simulations starting from a chemical composition formula;
 - Perform peak shaping based on centroid value, intensity and mass spectrometer resolving power;

