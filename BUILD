INSTALLATION ON DEBIAN/UBUNTU
=============================

sudo apt install minexpert2

#optionally install the user manual (HTML and PDF formats).
sudo apt install minexpert2-doc


BUILDING MINEXPERT2
===================

The mineXpert2 software build is under the control of the CMake build system.
There are a number of dependencies to install prior to trying to build the
software, as described below.


DEPENDENCIES
============

The dependencies to be installed are listed here with package names matching the
packages that are in Debian/Ubuntu. In other RPM-based software, most often the
package names are similar, albeit with some slight differences.

# The build system
cmake (>= 3.12),

# For the conversion of svg files to png.
graphicsmagick-imagemagick-compat,

# For the parallel computations
libgomp1,

# For the isotopic cluster calculations
libisospec++-dev (>=2.1.2),

# For all the raw mass calculations like 
# the data model, the mass spectral combinations...
libpappsomspp-dev (>= 0.8.8),
libpappsomspp-widget-dev (>= 0.8.8),

# For all the plotting
libqcustomplot-dev(>= 2.0),

# For the C++ objects (GUI and non-GUI)
qtbase5-dev (>= 5.11.3),
libqt5svg5-dev (>= 5.11.3),
qttools5-dev-tools,
qtchooser

# For the man page
docbook-to-man,

# For the documentation (optional, with -DMAKE_USER_MANUAL=1 as a flag to the
# cmake call, see below.
daps (>= 3.0.0)
libjeuclid-core-java,
libjeuclid-fop-java,
docbook-mathml,
libjs-jquery,
libjs-highlight.js,
libjs-mathjax,
fonts-mathjax,
fonts-mathjax-extras,
texlive-fonts-extra,
fonts-ebgaramond-extra,


GETTING THE SOURCE TARBALL
==========================

In the example below, the version of the software to be installed is 7.3.0.
Replace that version with any latest version of interest, which can be looked
for at https://gitlab.com/msxpertsuite/minexpert2/-/releases.


Using git
---------

The rather convoluted command below only downloads the branch of interest. The
whole git repos is very large...

git clone https://gitlab.com/msxpertsuite/minexpert2.git --branch master/7.3.0-1 --single-branch minexpert2-7.3.0

Using wget to download the tarball
----------------------------------

wget https://gitlab.com/msxpertsuite/minexpert2/-/archive/7.3.0/minexpert2-7.3.0.tar.gz

  Untar the tarball:

    tar xvzf minexpert2-7.3.0.tar.gz
  
Now start using the source tree to build the software
-----------------------------------------------------

  Change directory:

    cd minexpert2-7.3.0

  Create a build directory:

    mkdir build

  Change directory:

    cd build

  Configure the build:

    cmake ../ -DCMAKE_BUILD_TYPE=Release

  Build the software:

    make

  Run the software:

    src/minexpert2




