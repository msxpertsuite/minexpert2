[Setup]
AppName=@CMAKE_PROJECT_NAME@

; Set version number below
#define public version "@VERSION@"
AppVersion={#version}

#define public arch "mingw64"
#define public platform "win10"
#define public buildsystem "MXE"

;;;;;; On Wine, z: drive is configured to point to /home/rusconi
;;;;;; Check the winecfg window's Drives tab

#define sourceDir "z:/devel/@LOWCASE_PROJECT_NAME@/development"

#define buildDir "z:/devel/@LOWCASE_PROJECT_NAME@/build-area/mxe"

#define docBuildDir "z:/devel/@LOWCASE_PROJECT_NAME@/build-area/unix/doc/user-manual"

#define mxeDllDir "z:/devel/mxe/dlls-and-stuff-for-packages"


; Set version number below
AppVerName=@CMAKE_PROJECT_NAME@ version {#version}
DefaultDirName={commonpf}\@CMAKE_PROJECT_NAME@
DefaultGroupName=@CMAKE_PROJECT_NAME@
OutputDir="{#sourceDir}\winInstaller"

OutputBaseFilename=@CMAKE_PROJECT_NAME@-{#arch}-{#platform}-{#buildsystem}-v{#version}-setup

OutputManifestFile=@CMAKE_PROJECT_NAME@-{#arch}-{#platform}-v{#version}-setup-manifest.txt
ArchitecturesAllowed=x64
ArchitecturesInstallIn64BitMode=x64

LicenseFile="{#sourceDir}/LICENSE"
AppCopyright="Copyright (C) 2009-2023 Filippo Rusconi"

AllowNoIcons=yes
AlwaysShowComponentsList=yes
AllowRootDirectory=no
AllowCancelDuringInstall=yes
AppComments=@CMAKE_PROJECT_NAME@, by Filippo Rusconi"
AppContact="Filippo Rusconi, PhD, Research scientist at CNRS, France"
CloseApplications=yes
CreateUninstallRegKey=yes
DirExistsWarning=yes
WindowResizable=yes
WizardImageFile="{#sourceDir}\images\splashscreen-@LOWCASE_PROJECT_NAME@-innosetup.bmp"
WizardImageStretch=yes

[Dirs]
Name: "{app}\data"
Name: "{app}\doc"

[Files]
Source: "{#mxeDllDir}/*"; DestDir: {app}; Flags: ignoreversion recursesubdirs;

Source: "{#sourceDir}/doc/history.html"; DestDir: {app}\doc;

Source: "{#buildDir}/src/@CMAKE_PROJECT_NAME@.exe"; DestDir: {app};

Source: "{#docBuildDir}/@LOWCASE_PROJECT_NAME@-doc.pdf"; DestDir: {app}\doc;
Source: "{#docBuildDir}/html/user-manual/*"; Flags: recursesubdirs; DestDir: {app}\doc\html;

[Icons]
Name: "{group}\@CMAKE_PROJECT_NAME@"; Filename: "{app}\@CMAKE_PROJECT_NAME@.exe"; WorkingDir: "{app}"
Name: "{group}\Uninstall @CMAKE_PROJECT_NAME@"; Filename: "{uninstallexe}"

[Run]
Filename: "{app}\@CMAKE_PROJECT_NAME@.exe"; Description: "Launch @CMAKE_PROJECT_NAME@"; Flags: postinstall nowait unchecked

