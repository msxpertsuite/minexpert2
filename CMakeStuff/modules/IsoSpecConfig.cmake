#sudo apt install libisospec++-dev

set(IsoSpec_FOUND 1)
set(IsoSpec_INCLUDE_DIRS "/usr/include")
set(IsoSpec_LIBRARY "/usr/lib/libIsoSpec++.so.1.9.1") 
if(NOT TARGET IsoSpec::IsoSpec)
	add_library(IsoSpec::IsoSpec UNKNOWN IMPORTED)
	set_target_properties(IsoSpec::IsoSpec PROPERTIES
		IMPORTED_LOCATION             "${IsoSpec_LIBRARY}"
		INTERFACE_INCLUDE_DIRECTORIES "${IsoSpec_INCLUDE_DIRS}")
endif()


