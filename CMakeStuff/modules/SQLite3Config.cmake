#[=======================================================================[.rst:
FindSQLite3
-----------

Find the SQLite libraries, v3

IMPORTED targets
^^^^^^^^^^^^^^^^

This module defines the following :prop_tgt:`IMPORTED` target:

``SQLite::SQLite3``

Result variables
^^^^^^^^^^^^^^^^

This module will set the following variables if found:

``SQLite3_INCLUDE_DIRS``
  where to find sqlite3.h, etc.
``SQLite3_LIBRARIES``
  the libraries to link against to use SQLite3.
``SQLite3_VERSION``
  version of the SQLite3 library found
``SQLite3_FOUND``
  TRUE if found

#]=======================================================================]

#sudo apt install libsqlite3-dev
    
# Look for the necessary header
set(SQLite3_INCLUDE_DIR /usr/include)
mark_as_advanced(SQLite3_INCLUDE_DIR)

# Look for the necessary library
set(SQLite3_LIBRARY /usr/lib/x86_64-linux-gnu/libsqlite3.so.0.8.6)
mark_as_advanced(SQLite3_LIBRARY)

set(SQLite3_FOUND 1)

set(SQLite3_VERSION 0.8.6)

    set(SQLite3_INCLUDE_DIRS ${SQLite3_INCLUDE_DIR})
    set(SQLite3_LIBRARIES ${SQLite3_LIBRARY})
    if(NOT TARGET SQLite::SQLite3)
        add_library(SQLite::SQLite3 UNKNOWN IMPORTED)
        set_target_properties(SQLite::SQLite3 PROPERTIES
            IMPORTED_LOCATION             "${SQLite3_LIBRARY}"
            INTERFACE_INCLUDE_DIRECTORIES "${SQLite3_INCLUDE_DIR}")
    endif()
    

