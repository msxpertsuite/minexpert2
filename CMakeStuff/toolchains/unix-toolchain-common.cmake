message(STATUS "UNIX toolchain")

set(CMAKE_C_IMPLICIT_INCLUDE_DIRECTORIES /usr/include)
set(CMAKE_CXX_IMPLICIT_INCLUDE_DIRECTORIES /usr/include)

# This is used throughout all the build system files
set(TARGET ${LOWCASE_PROJECT_NAME})

# Find the libIsoSpec library
find_package(IsoSpec++ GLOBAL REQUIRED)

find_package(QCustomPlotQt6 REQUIRED)
# Per instructions of the lib author:
# https://www.qcustomplot.com/index.php/tutorials/settingup
message(STATUS "Setting definition -DQCUSTOMPLOT_USE_LIBRARY.")
# message(STATUS "QCustomPlotQt6_LIBRARIES: ${QCustomPlotQt6_LIBRARIES}")
# message(STATUS "QCustomPlotQt6_INCLUDE_DIR: ${QCustomPlotQt6_INCLUDE_DIR}")

find_package(QuaZip-Qt6 REQUIRED)

if(PAPPSO_LOCAL_DEV)
  include(${CMAKE_TOOLCHAINS_PATH}/unix-toolchain-local-pappso.cmake)
else()
find_package(PappsoMSpp COMPONENTS Core Widget GLOBAL REQUIRED)
endif()


if(XPERTMASS_LOCAL_DEV)
  include(${CMAKE_TOOLCHAINS_PATH}/unix-toolchain-local-xpertmass.cmake)
else()
  find_package(XpertMass COMPONENTS Core Gui GLOBAL REQUIRED)
endif()

#OPENMP
message(STATUS "${BoldYellow}OpenMP support is compulsory.${ColourReset}")
#message(STATUS "CMAKE_MODULE_PATH: ${CMAKE_MODULE_PATH}")
#set(OpenMP_DIR ${CMAKE_MODULE_PATH})

find_package(OpenMP REQUIRED)

#We know this one is empty:
#message(STATUS "OpenMP found with include dirs: ${OpenMP_CXX_INCLUDE_DIRS}")

#Which is why we will reconstruct it for gcc
#on the basis of the path to the libgomp.so library.The include
#directory is nothing more than the path to that library with '/include'
#added to it.This is what is done below.
message(STATUS "OpenMP found with libraries: ${OpenMP_CXX_LIBRARIES}")

# On mingw64 with ucrt64 environment, the include file is
# /c/msys64/ucrt64/lib/gcc/x86_64-w64-mingw32/13.2.0/include/omp.h

if(OpenMP_CXX_INCLUDE_DIRS STREQUAL "")

  message(STATUS "OpenMP include directories could not be found.
  Crafting them manually starting from the library directories.")

  if(CMAKE_COMPILER_IS_GNUCXX)

    message(STATUS "The compiler is CMAKE_COMPILER_IS_GNUCXX")

    #We now need to extract the libgomp.so library path, and craft the
    #include dir path which is replacing libgomp.so with include.
    set(OpenMP_CXX_INCLUDE_DIRS ${OpenMP_CXX_LIBRARIES})
    list(FILTER OpenMP_CXX_INCLUDE_DIRS INCLUDE REGEX ".*libgomp.*")
    #message(STATUS "Started crafting OpenMP include dirs: ${OpenMP_CXX_INCLUDE_DIRS}")
    list(LENGTH OpenMP_CXX_INCLUDE_DIRS INCLUDE_DIRS_LIST_LENGTH)
    #message(STATUS "Remaining items in OpenMP_CXX_INCLUDE_DIRS: ${INCLUDE_DIRS_LIST_LENGTH}")
    list(GET OpenMP_CXX_INCLUDE_DIRS 0 OpenMP_CXX_INCLUDE_DIR)
    #message(STATUS "Started crafting OpenMP include dir: ${OpenMP_CXX_INCLUDE_DIR}")
    string(REGEX REPLACE "libgomp.so.*" "include" OpenMP_CXX_INCLUDE_SINGLE_DIR
${OpenMP_CXX_INCLUDE_DIR})
    message(STATUS "OpenMP single include dir: ${OpenMP_CXX_INCLUDE_SINGLE_DIR}")

    if(EXISTS "${OpenMP_CXX_INCLUDE_SINGLE_DIR}")
      set(OpenMP_CXX_INCLUDE_DIRS ${OpenMP_CXX_INCLUDE_SINGLE_DIR})
      message(STATUS "OpenMP found with include dirs: ${OpenMP_CXX_INCLUDE_DIRS}")
    else()
      set(OpenMP_CXX_INCLUDE_DIRS "")
      message(STATUS "OpenMP found but not the include directories.")
    endif()

  else() # Compiler is not CMAKE_COMPILER_IS_GNUCXX

    # Check if the compiler is clang++

    #set(DEBUG_COMPILER "/usr/bin/clang")
    #string(REGEX MATCH ".*clang\\+\\+.*" CMAKE_CXX_COMPILER_IS_CLANG ${DEBUG_COMPILER})

    string(REGEX MATCH ".*clang\\+\\+.*" CMAKE_CXX_COMPILER_IS_CLANG ${CMAKE_CXX_COMPILER})
    #string(FIND ${CMAKE_CXX_COMPILER} "clang++" CMAKE_CXX_COMPILER_IS_CLANG)

    if(CMAKE_CXX_COMPILER_IS_CLANG STREQUAL "")

      message(STATUS "Failed to identify compiler different than g++ and clang++")

    else()

      message(STATUS "The compiler has been identified as clang++: ${CMAKE_CXX_COMPILER}")

      execute_process (
        COMMAND bash -c "clang --print-resource-dir"
        OUTPUT_STRIP_TRAILING_WHITESPACE
        OUTPUT_VARIABLE CLANG_RESOURCE_DIR
      )

      if(${CLANG_RESOURCE_DIR} STREQUAL "")

        message(STATUS "Failed to determine the Clang resource dir")

      else()

        #message(STATUS "Clang resource dir: \"${CLANG_RESOURCE_DIR}\"")
        set(CLANG_RESOURCE_DIR "${CLANG_RESOURCE_DIR}/include")
        #message(STATUS "Clang resource dir: ${CLANG_RESOURCE_DIR}")

        if(EXISTS "${CLANG_RESOURCE_DIR}")
          set(OpenMP_CXX_INCLUDE_DIRS ${CLANG_RESOURCE_DIR})
          message(STATUS "OpenMP found with include dirs: ${OpenMP_CXX_INCLUDE_DIRS}")
        else()
          set(OpenMP_CXX_INCLUDE_DIRS "")
          message(STATUS "OpenMP found but not the include directories.")
        endif()

      endif()

    endif()

  endif()

endif()

## INSTALL directories
set(BIN_DIR ${CMAKE_INSTALL_PREFIX}/bin)
set(DOC_DIR ${CMAKE_INSTALL_PREFIX}/share/doc/${TARGET})


# The appstream, desktop and icon files
install(FILES org.msxpertsuite.${TARGET}.desktop
  DESTINATION ${CMAKE_INSTALL_PREFIX}/share/applications)

install(FILES org.msxpertsuite.${TARGET}.appdata.xml
  DESTINATION ${CMAKE_INSTALL_PREFIX}/share/metainfo)

install(FILES images/icons/16x16/${TARGET}.png
  DESTINATION
  ${CMAKE_INSTALL_PREFIX}/share/icons/hicolor/16x16/apps)

install(FILES images/icons/32x32/${TARGET}.png
  DESTINATION
  ${CMAKE_INSTALL_PREFIX}/share/icons/hicolor/32x32/apps)

install(FILES images/icons/48x48/${TARGET}.png
  DESTINATION
  ${CMAKE_INSTALL_PREFIX}/share/icons/hicolor/48x48/apps)

install(FILES images/icons/64x64/${TARGET}.png
  DESTINATION
  ${CMAKE_INSTALL_PREFIX}/share/icons/hicolor/64x64/apps)


# Prepare the AppImage recipe file to be used elsewhere,
# for deploying mineXpert2 on non-Debian- or non-Ubuntu platforms.

configure_file(${CMAKE_SOURCE_DIR}/CMakeStuff/${CMAKE_PROJECT_NAME}-appimage-recipe.yml.in
  ${CMAKE_SOURCE_DIR}/appimage/${CMAKE_PROJECT_NAME}-appimage-recipe.yml @ONLY)

## Platform-dependent compiler flags:
include(CheckCXXCompilerFlag)

if (WITH_FPIC)
  add_definitions(-fPIC)
endif()
