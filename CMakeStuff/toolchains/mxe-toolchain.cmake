# This file should be included if the command line reads like this:
# x86_64-w64-mingw32.shared-cmake -DCMAKE_BUILD_TYPE=Release -DMXE=1 ..

MESSAGE("MXE (M cross environment) https://mxe.cc/")
message("Please run the configuration like this:")
message("x86_64-w64-mingw32.shared-cmake -G \"Unix Makefiles\" -DCMAKE_BUILD_TYPE=Release ../../development")

set(HOME_DEVEL_DIR $ENV{HOME}/devel)
set(MXE_SHIPPED_DLLS_DIR "$ENV{HOME}/devel/mxe/dlls-and-stuff-for-packages")

set(CMAKE_C_IMPLICIT_INCLUDE_DIRECTORIES ${HOME_DEVEL_DIR}/mxe/usr/x86_64-w64-mingw32.shared/include)
set(CMAKE_CXX_IMPLICIT_INCLUDE_DIRECTORIES ${HOME_DEVEL_DIR}/mxe/usr/x86_64-w64-mingw32.shared/include)

# This is used throughout all the build system files
set(TARGET ${CMAKE_PROJECT_NAME})

# Now that we have the TARGET variable contents, let's configure InnoSetup
CONFIGURE_FILE(${CMAKE_SOURCE_DIR}/CMakeStuff/${LOWCASE_PROJECT_NAME}-mxe.iss.in
  ${CMAKE_SOURCE_DIR}/winInstaller/${LOWCASE_PROJECT_NAME}-mxe.iss @ONLY)

find_package(Qt6 COMPONENTS Widgets Xml Svg Sql SvgWidgets PrintSupport Network Concurrent GLOBAL REQUIRED)


set(IsoSpec++_FOUND 1)
set(IsoSpec++_INCLUDE_DIRS "${HOME_DEVEL_DIR}/isospec/development")
set(IsoSpec++_LIBRARIES "${MXE_SHIPPED_DLLS_DIR}/libIsoSpec++.dll")
if(NOT TARGET IsoSpec++::IsoSpec++)
  add_library(IsoSpec++::IsoSpec++ UNKNOWN IMPORTED)
  set_target_properties(IsoSpec++::IsoSpec++ PROPERTIES
    IMPORTED_LOCATION             "${IsoSpec++_LIBRARIES}"
    INTERFACE_INCLUDE_DIRECTORIES "${IsoSpec++_INCLUDE_DIRS}")
endif()


set(QCustomPlotQt6_FOUND 1)
set(QCustomPlotQt6_INCLUDE_DIRS "${HOME_DEVEL_DIR}/qcustomplot/development")
# Note the QCustomPlotQt6_LIBRARIES (plural) because on Debian, the
# QCustomPlotQt6Config.cmake file has this variable name (see the unix-specific
# toolchain file.
set(QCustomPlotQt6_LIBRARIES "${HOME_DEVEL_DIR}/qcustomplot/build-area/mxe/libQCustomPlotQt6.dll")
# Per instructions of the lib author:
# https://www.qcustomplot.com/index.php/tutorials/settingup
message(STATUS "Setting definition -DQCUSTOMPLOT_USE_LIBRARY.")
if(NOT TARGET QCustomPlotQt6::QCustomPlotQt6)
	add_library(QCustomPlotQt6::QCustomPlotQt6 UNKNOWN IMPORTED)
	set_target_properties(QCustomPlotQt6::QCustomPlotQt6 PROPERTIES
		IMPORTED_LOCATION             "${QCustomPlotQt6_LIBRARIES}"
		INTERFACE_INCLUDE_DIRECTORIES "${QCustomPlotQt6_INCLUDE_DIRS}"
		INTERFACE_COMPILE_DEFINITIONS QCUSTOMPLOT_USE_LIBRARY
		)
endif()


# Unfortunately no CMake configuration file is available.
# find_package(QUAZIP REQUIRED)

set(QUAZIP_FOUND 1)
set(QUAZIP_INCLUDE_DIR "${HOME_DEVEL_DIR}/quazip/development/quazip")
set(QUAZIP_LIBRARIES "${HOME_DEVEL_DIR}/quazip/build-area/mxe/quazip/libquazip1-qt6.dll")
set(QUAZIP_ZLIB_INCLUDE_DIR ${ZLIB_INCLUDE_DIRS})
set(QUAZIP_INCLUDE_DIRS ${QUAZIP_INCLUDE_DIR} ${QUAZIP_ZLIB_INCLUDE_DIR})

message(STATUS "QUAZIP_INCLUDE_DIR :${QUAZIP_INCLUDE_DIR}")

if(NOT TARGET QuaZip::QuaZip)
	add_library(QuaZip::QuaZip UNKNOWN IMPORTED)
	set_target_properties(QuaZip::QuaZip PROPERTIES
		IMPORTED_LOCATION             ${QUAZIP_LIBRARIES}
		INTERFACE_INCLUDE_DIRECTORIES ${QUAZIP_INCLUDE_DIR})
endif()

set(PappsoMSpp_FOUND 1)
set(PappsoMSpp_INCLUDE_DIRS "${HOME_DEVEL_DIR}/pappsomspp/development/src")
set(PappsoMSpp_LIBRARIES
  "${MXE_SHIPPED_DLLS_DIR}/libpappsomspp.dll")
if(NOT TARGET PappsoMSpp::Core)

  add_library(PappsoMSpp::Core UNKNOWN IMPORTED GLOBAL)
  set_target_properties(PappsoMSpp::Core PROPERTIES
    IMPORTED_LOCATION ${PappsoMSpp_LIBRARIES}
    INTERFACE_INCLUDE_DIRECTORIES ${PappsoMSpp_INCLUDE_DIRS})

endif()


set(PappsoMSppWidget_FOUND 1)
set(PappsoMSppWidget_LIBRARIES
  "${MXE_SHIPPED_DLLS_DIR}/libpappsomspp-widget.dll")
if(NOT TARGET PappsoMSpp::Widget)

  add_library(PappsoMSpp::Widget UNKNOWN IMPORTED GLOBAL)
  set_target_properties(PappsoMSpp::Widget PROPERTIES
    IMPORTED_LOCATION ${PappsoMSppWidget_LIBRARIES}
    INTERFACE_INCLUDE_DIRECTORIES ${PappsoMSpp_INCLUDE_DIRS})

endif()

include_directories(${include_directories} ${PappsoMSpp_INCLUDE_DIRS} ${PappsoMSpp_INCLUDE_DIRS})


# Find the libmass static library
set(libmass_FOUND 1)
# Below: for the generated UIS_H files and also to access the includes like
# #include <libmass/_file_> or #include <_file_>
set(libmass_INCLUDE_DIRS "${CMAKE_SOURCE_DIR}/libmass/includes"
  "${CMAKE_SOURCE_DIR}/libmass/includes/libmass" "${CMAKE_BINARY_DIR}/libmass")
set(libmass_LIBRARIES "${CMAKE_SOURCE_DIR}/libmass/libmass.a")
if(NOT TARGET libmass::nongui)

  add_library(libmass::nongui UNKNOWN IMPORTED)

  set_target_properties(libmass::nongui PROPERTIES
    IMPORTED_LOCATION             "${libmass_LIBRARIES}"
    INTERFACE_INCLUDE_DIRECTORIES "${libmass_INCLUDE_DIRS}"
  )
endif()

message(STATUS "Found libmass at: ${libmass_LIBRARIES} with include dir: ${libmass_INCLUDE_DIRS}")

# Find the libmassgui static library
set(libmassgui_FOUND 1)
# Below: for the generated UIS_H files and also to access the includes like
# #include <libmassgui/_file_> or #include <_file_>
set(libmassgui_INCLUDE_DIRS "${CMAKE_SOURCE_DIR}/libmassgui/includes"
  "${CMAKE_SOURCE_DIR}/libmassgui/includes/libmassgui" "${CMAKE_BINARY_DIR}/libmassgui")
set(libmassgui_LIBRARIES "${CMAKE_SOURCE_DIR}/libmassgui/libmassgui.a")
if(NOT TARGET libmass::gui)

  add_library(libmass::gui UNKNOWN IMPORTED)

  set_target_properties(libmass::gui PROPERTIES
    IMPORTED_LOCATION             "${libmassgui_LIBRARIES}"
    INTERFACE_INCLUDE_DIRECTORIES "${libmassgui_INCLUDE_DIRS}"
  )
endif()

message(STATUS "Found libmassgui at: ${libmassgui_LIBRARIES} with include dir: ${libmassgui_INCLUDE_DIRS}")


#OPENMP
message(STATUS "${BoldYellow}OpenMP support is compulsory.${ColourReset}")
#message(STATUS "CMAKE_MODULE_PATH: ${CMAKE_MODULE_PATH}")
#set(OpenMP_DIR ${CMAKE_MODULE_PATH})

find_package(OpenMP REQUIRED)


## We can build the package setup executable with this specific command.
add_custom_target(winpackage
      COMMAND wine $ENV{HOME}/.wine/drive_c/Program\ Files\ \(x86\)/Inno\ Setup\ 6/Compil32.exe /cc "z:devel/${LOWCASE_PROJECT_NAME}/development/winInstaller/${LOWCASE_PROJECT_NAME}-mxe.iss"
      WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}/winInstaller
      COMMENT "Build of the Windows Package Setup executable"
      VERBATIM)


## INSTALL directories
# This is the default on windows, but set it nonetheless.
set(CMAKE_INSTALL_PREFIX "C:/Program Files")

set(BIN_DIR ${CMAKE_INSTALL_PREFIX}/${CMAKE_PROJECT_NAME})

set(DOC_DIR ${CMAKE_INSTALL_PREFIX}/${CMAKE_PROJECT_NAME}/doc)

# On Win10 all the code is relocatable.
remove_definitions(-fPIC)


