message("WIN10-MINGW64 environment https://www.msys2.org/")
message("Please run the configuration like this:")
message("cmake -G \"Unix Makefiles\" -DCMAKE_BUILD_TYPE=Debug ../development")

# Comment out these two lines that make the build fail with #include_next math.h
# file not found error.
# set(CMAKE_C_IMPLICIT_INCLUDE_DIRECTORIES "c:/msys64/mingw64/include")
# set(CMAKE_CXX_IMPLICIT_INCLUDE_DIRECTORIES "c:/msys64/mingw64/include")

# This is used throughout all the build system files
set(TARGET ${CMAKE_PROJECT_NAME})

set(HOME_DEVEL_DIR "$ENV{HOME}/devel")

CONFIGURE_FILE(${CMAKE_SOURCE_DIR}/CMakeStuff/minexpert2-msys64.iss.in
  ${CMAKE_SOURCE_DIR}/winInstaller/minexpert2-msys64.iss @ONLY)

# Sql below for pappsomspp Bruker timsTOF data handling
find_package(Qt6 COMPONENTS Widgets Xml Svg PrintSupport Network Sql Concurrent GLOBAL REQUIRED)


set(QCustomPlotQt6_FOUND 1)
set(QCustomPlotQt6_INCLUDE_DIRS "${HOME_DEVEL_DIR}/qcustomplot/development")
# Note the QCustomPlotQt6_LIBRARIES (plural) because on Debian, the
# QCustomPlotQt6Config.cmake file has this variable name (see the unix-specific
# toolchain file.
set(QCustomPlotQt6_LIBRARIES "${HOME_DEVEL_DIR}/qcustomplot/build-area/mingw64/libQCustomPlotQt6.dll")
# Per instructions of the lib author:
# https://www.qcustomplot.com/index.php/tutorials/settingup
message(STATUS "Setting definition -DQCUSTOMPLOT_USE_LIBRARY.")
if(NOT TARGET QCustomPlotQt6::QCustomPlotQt6)
	add_library(QCustomPlotQt6::QCustomPlotQt6 UNKNOWN IMPORTED)
	set_target_properties(QCustomPlotQt6::QCustomPlotQt6 PROPERTIES
		IMPORTED_LOCATION             "${QCustomPlotQt6_LIBRARIES}"
		INTERFACE_INCLUDE_DIRECTORIES "${QCustomPlotQt6_INCLUDE_DIRS}"
		INTERFACE_COMPILE_DEFINITIONS QCUSTOMPLOT_USE_LIBRARY
		)
endif()


set(IsoSpec++_FOUND 1)
set(IsoSpec++_INCLUDE_DIRS "${HOME_DEVEL_DIR}/isospec/development")
set(IsoSpec++_LIBRARIES "${HOME_DEVEL_DIR}/isospec/build-area/mingw64/IsoSpec++/libIsoSpec++.dll")
if(NOT TARGET IsoSpec++::IsoSpec++)
  add_library(IsoSpec++::IsoSpec++ UNKNOWN IMPORTED)
  set_target_properties(IsoSpec++::IsoSpec++ PROPERTIES
    IMPORTED_LOCATION             "${IsoSpec++_LIBRARIES}"
    INTERFACE_INCLUDE_DIRECTORIES "${IsoSpec++_INCLUDE_DIRS}")
endif()


find_package(QuaZip-Qt6 REQUIRED)


set(PwizLite_FOUND 1)
set(PwizLite_INCLUDE_DIRS "${HOME_DEVEL_DIR}/pwizlite/development/src")
set(PwizLite_LIBRARIES "${HOME_DEVEL_DIR}/pwizlite/build-area/mingw64/src/libpwizlite.dll")
if(NOT TARGET PwizLite::PwizLite)

  add_library(PwizLite::PwizLite UNKNOWN IMPORTED)

  set_target_properties(PwizLite::PwizLite PROPERTIES
    IMPORTED_LOCATION             "${PwizLite_LIBRARIES}"
    INTERFACE_INCLUDE_DIRECTORIES "${PwizLite_INCLUDE_DIRS}"
    )
endif()


set(PappsoMSpp_FOUND 1)
set(PappsoMSpp_INCLUDE_DIRS "${HOME_DEVEL_DIR}/pappsomspp/development/src")
set(PappsoMSpp_LIBRARIES
  "${HOME_DEVEL_DIR}/pappsomspp/build-area/mingw64/src/libpappsomspp.dll")
if(NOT TARGET PappsoMSpp::Core)

  add_library(PappsoMSpp::Core UNKNOWN IMPORTED GLOBAL)
  set_target_properties(PappsoMSpp::Core PROPERTIES
    IMPORTED_LOCATION ${PappsoMSpp_LIBRARIES}
    INTERFACE_INCLUDE_DIRECTORIES ${PappsoMSpp_INCLUDE_DIRS})

endif()


set(PappsoMSppWidget_FOUND 1)
set(PappsoMSppWidget_LIBRARIES
  "${HOME_DEVEL_DIR}/pappsomspp/build-area/mingw64/src/pappsomspp/widget/libpappsomspp-widget.dll")
if(NOT TARGET PappsoMSpp::Widget)

  add_library(PappsoMSpp::Widget UNKNOWN IMPORTED GLOBAL)
  set_target_properties(PappsoMSpp::Widget PROPERTIES
    IMPORTED_LOCATION ${PappsoMSppWidget_LIBRARIES}
    INTERFACE_INCLUDE_DIRECTORIES ${PappsoMSpp_INCLUDE_DIRS})

endif()

include_directories(${include_directories} ${PappsoMSpp_INCLUDE_DIRS} ${PappsoMSpp_INCLUDE_DIRS})


# The libmass and libmassgui libraries are made available
# right from their building directory. No need to search for
# them here.


#OPENMP
message(STATUS "${BoldYellow}OpenMP support is compulsory.${ColourReset}")
#message(STATUS "CMAKE_MODULE_PATH: ${CMAKE_MODULE_PATH}")
#set(OpenMP_DIR ${CMAKE_MODULE_PATH})

# Unfortunately that does not work as it does not find the
# include directories. So we need to do the work manually.

find_package(OpenMP REQUIRED)

set(OpenMP_FOUND 1)
set(OpenMP_INCLUDE_DIRS "C:/msys64/ucrt64/lib/gcc/x86_64-w64-mingw32/13.2.0/include/omp.h")
set(OpenMP_LIBRARIES "C:/msys64/ucrt64/bin/libgomp-1.dll")
if(NOT TARGET OpenMP::OpenMP_CXX)

  add_library(OpenMP::OpenMP UNKNOWN IMPORTED)

  set_target_properties(OpenMP::OpenMP_CXX PROPERTIES
    IMPORTED_LOCATION             "${OpenMP_LIBRARIES}"
    INTERFACE_INCLUDE_DIRECTORIES "${OpenMP_INCLUDE_DIRS}"
    )
endif()

message(STATUS "OpenMP libraries: ${OpenMP_LIBRARIES}")
message(STATUS "OpenMP include dirs: ${OpenMP_INCLUDE_DIRS}")


## INSTALL directories
# This is the default on windows, but set it nonetheless.
set(CMAKE_INSTALL_PREFIX "C:/Program Files")
set(BIN_DIR ${CMAKE_INSTALL_PREFIX}/${TARGET})
# On Win, the doc dir is uppercase.
set(DOC_DIR ${CMAKE_INSTALL_PREFIX}/${TARGET}/doc)

# Resource file for the win icon.
if(NOT CMAKE_RC_COMPILER)
  set(CMAKE_RC_COMPILER windres.exe)
endif()

execute_process(COMMAND ${CMAKE_RC_COMPILER}
  -D GCC_WINDRES
  -I "${CMAKE_CURRENT_SOURCE_DIR}"
  -i "${CMAKE_SOURCE_DIR}/${LOWCASE_PROJECT_NAME}.rc"
  -o "${CMAKE_CURRENT_BINARY_DIR}/${LOWCASE_PROJECT_NAME}.obj"
  WORKING_DIRECTORY	${CMAKE_CURRENT_SOURCE_DIR})


# On Win10 all the code is relocatable.
remove_definitions(-fPIC)


