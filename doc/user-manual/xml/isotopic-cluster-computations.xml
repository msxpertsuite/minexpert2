<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE chapter [

<!ENTITY % entities SYSTEM "entities.ent">
%entities;
]>

<chapter xml:id="chap_isotopic-cluster-calculations"
xmlns="http://docbook.org/ns/docbook" version="5.0"
xmlns:xlink="http://www.w3.org/1999/xlink"> 

<info>

	<title>Isotopic cluster calculations</title> 

	<keywordset> 

		<keyword>libIsoSpec++</keyword> 

		<keyword>isotopic cluster</keyword> 

	</keywordset> 

</info>


<sect1 xml:id="sec_calculating-isotopic-clusters-with-isospec">

	<title>Calculating Isotopic Clusters with IsoSpec</title>

	<para>

		When the resolution of the mass spectrometer is good, zooming-in on a mass
		peak may reveal that a given ion has given rise not to one peak but to a set
		of peaks. This set of peaks is called an <quote><emphasis>isotopic
		cluster</emphasis></quote>.

	</para>


	<para>

		It is possible to predict how a given ion (of known chemical formula) is
		supposed to be revealed in a mass spectrum, in the form of such an isotopic
		cluster. One such cluster is shown in <xref
		linkend="fig_minexpert-calculated-isotopic-cluster-mass-spectrum"/>, for the
		horse apomyoglobin protein in its monoprotonated form
		[M+H<superscript>+</superscript>]<superscript>+</superscript>, of elemental
		composition C769H1213N210O218S2 (this formula is typeset like this
		intentionally, to show how the formul&aelig; need to be entered in the
		IsoSpec module).

	</para>


	<figure xml:id="fig_minexpert-calculated-isotopic-cluster-mass-spectrum">

		<title>Calculation of the isotopic cluster of an analyte</title>

		<mediaobject>

			<imageobject role="fo">
				<imagedata fileref="print-minexpert-calculated-isotopic-cluster-mass-spectrum.png"/>
			</imageobject>
			<imageobject role="html">
				<imagedata fileref="web-minexpert-calculated-isotopic-cluster-mass-spectrum.png"/>
			</imageobject>

			<caption><para>

					Calculated isotopic cluster for the monoprotonated
					apomyoglobin protein.

				</para>
			</caption>

		</mediaobject>

	</figure>

	<para>

		&mineXp2; provides an interface to the <filename
		class="libraryfile">libIsoSpec++</filename> library.

		<blockquote>
			<para>
				IsoSpec: Hyperfast Fine Structure Calculator</para>
			<para>

				Mateusz K. Łącki, Michał Startek, Dirk Valkenborg, and Anna Gambin</para>
			<para>

				<emphasis>Analytical Chemistry</emphasis>, 2017, 89, 3272&ndash;3277</para>
			<para>

				DOI: 10.1021/acs.analchem.6b01459</para>
		</blockquote>	

		This library performs high-resolution isotopic cluster calculations. In order
		to run the calculations, it is necessary to have the following items ready:

		<itemizedlist>

			<listitem> <para>

					An elemental composition formula of the analyte (for example,
					H2O1).  This formula needs to account for the ionization agent that is
					involved in the ionization of the analyte prior to its detection in the mass
					spectrometer.

				</para>


				<important>

					<para>

						The <productname>IsoSpec</productname> software requires that all the
						chemical elements of a chemical formula be indexed. This means that, for
						water, for example, the formula should be H2O1 (notice the index 1 after the O
						element symbol).

					</para>


				</important>

			</listitem>

			<listitem> <para>

					A detailed isotopic configuration of all the chemical elements
					that are used in the elemental composition formula. &mineXp2; provides two
					interfaces to define the isotopic characteristics of the chemical elements.
					These will be described in the following sections.

				</para>
			</listitem>
		</itemizedlist>	

	</para>


	<sect2 xml:id="sec_graphical-user-interface-to-libisospec">

		<title>The IsoSpec Graphical User Interface in mineXpert</title>

		<para>

			Generating iosotopic clusters using the <productname>IsoSpec</productname>
			software package is not easily carried over, in particular because this
			remarkable library is designed to be highly performant. The authors
			rightfully put their energy into optimizations for accuracy and speed
			instead of investing in a graphical user interface. &mineXp2; provides that
			graphical user interface, shown in <xref
			linkend="fig_minexpert-isospec-cluster-calculation-dlg"/>, that shows up
			upon selection of the program's main window's

			<menuchoice>
				<guimenu>Utilities</guimenu>
				<guimenuitem>Isotopic cluster
				calculations</guimenuitem>
			</menuchoice> menu.

		</para>



		<figure xml:id="fig_minexpert-isospec-cluster-calculation-dlg">

			<title>Isotopic cluster calculation dialog window</title>

			<mediaobject>

				<imageobject role="fo">
					<imagedata fileref="print-minexpert-isospec-cluster-calculation-dlg.png"/>
				</imageobject>
				<imageobject role="html">
					<imagedata fileref="web-minexpert-isospec-cluster-calculation-dlg.png"/>
				</imageobject>

				<caption><para>

						The dialog window contains two panels. The left hand side panel
						configures the charge for which the calculation is to be carried over and
						the maximum cumulative isotopic presence probability that
						<productname>IsoSpec</productname> must reach during the calculation.  The
						right hand side panel contains a tab widget that contains the configuration
						tabs and the results tab.

					</para>
				</caption>

			</mediaobject>

		</figure>

		<para>

			An isotopic cluster calculation is most probably performed with the aim of
			simulating an expected isotopic cluster for an analyte that is being analyzed
			by mass spectrometry. It is thus logical that the analyte be in an ionized
			form. The way that the analyte has been ionized needs to be taken into account
			in the chemical formula that describes the ion for which the isotopic cluster
			is being calculated. For example, when determining the chemical formula of a
			protein in the positive ion mode, the number of protons used to ionize the
			protein need to be included in the analyte elemental composition
			formula.

		</para>


		<warning>

			<para>

				The <productname>IsoSpec</productname> software is
				<quote>charge-agnostic</quote> in the sense that it does not know what element
				in the chemical formula is responsible for the ionization of the analyte.
				Therefore, <productname>IsoSpec</productname> does not know of (and does not
				care about) the charge of the analyte. The ionization level of the analyte can
				be handled by &mineXp2; if that information is set to the <guilabel>Ion
				charge</guilabel> spin box widget. By default, the charge state of the
				analyte is 1.

			</para>


		</warning>

		<para>

			The <guilabel>Max. cumulative probability</guilabel> spin box widget
			serves to configure the extent to which <productname>IsoSpec</productname>
			simulates the theoretically expected isotopic cluster. A value of
			<guilabel>0.99</guilabel> tells the software to simulate enough combinations
			of the analyte isotopes to represent 99&nbsp;% of the theoretically expected
			combinations.

		</para>


		<tip>

			<para>

				For large biopolymers, it might be prudent to start with a relatively
				low value for <guilabel>Max. cumulative probability</guilabel>, because
				setting this value too high, that is, near 1, would increase notably the
				calculation duration.  

			</para>

		</tip>

		<para>

			To perform isotopic cluster calculations, the simulation software needs to
			be aware of all the isotopes of all the chemical elements that enter in
			the composition of the ionized analyte. An isotope is defined by its mass
			and by the probability that it is found in nature. Carbon has two major
			isotopes that can be found in nature: the &c12; most abundant isotope and
			the &c13; least abundant isotope (the &c14; isotope is irrelevant for
			conventional mass spectrometry unless molecules have been artificially
			enriched in that isotope).

		</para>


		<para>

			There are three ways that the user might use the IsoSpec software
			package. Two of them involve a configuration preparation on the part of the
			user. The third one, that we'll describe first, does not involve any
			chemical element configuration. The other ways are reviewed in the next
			sections.

		</para>


		<sect3 xml:id="sec_static_element-tables-shipped-within-isospec">

			<title>Static Standard Element Tables Shipped within the <productname>IsoSpec</productname>
				Library</title> 

			<para>

        In order to document all the chemical elements' isotopes'
        characteristics, the <productname>IsoSpec</productname> library has, in
        its own headers, a number of arrays that &mineXp2; automatically loads
        up when opening the <xref
        linkend="fig_minexpert-isospec-cluster-calculation-dlg"/> dialog
        window.<footnote><para>That process is performed at build time and is
        thus not configurable at run time.</para></footnote> These data are
        displayed in the <guilabel>IsoSpec standard static data</guilabel> tab.
        The table view widget is not editable, hence the
        <emphasis>static</emphasis> qualifier in the tab name.

			</para>


			<para>

				In this tab, all the user has to do is enter the formula for which
				the isotopic cluster calculation is to be performed. The formula needs
				to be pasted in the <guilabel>Formula</guilabel> line edit
				widget.

			</para>


			<para>

				Once the formula has been set to its line edit widget, configure the
				charge of the ion (<guilabel>Ion charge</guilabel> spin box widget) and the
				<guilabel>Max. cumulative probability</guilabel>. Now, click
				<guibutton>Run</guibutton>.

			</para>


		</sect3>

		<sect3 xml:id="sec_modified_element-tables-shipped-within-isospec">

			<title>Modified Standard Element Tables Shipped within the <productname>IsoSpec</productname>
				Library</title> 

			<para>

				While the previous section showed how to use the static element tables
				from the <productname>IsoSpec</productname> library, this section shows a
				way to configure these elemental data. For this, start from the standard
				IsoSpec-based data and modify them according to one's needs. To proceed,
				select all the element rows of interest from the <guilabel>IsoSpec standard
				static data</guilabel> tab and click <guibutton>Save selected to table
				file</guibutton>.

			</para>


			<tip>

				<para>

          The order with which the rows are selected is respected in the export,
          so make sure to select the rows in the order that makes sense for you.
          Also, be sure to select rows and not only some cells; to achieve, this
          click in the left margin of the table view widget.

				</para>


			</tip>

			<para>

				The export is performed as a text <acronym>TSV</acronym> (tab-separated
				value format) file in a layout that closely mimicks the data visible in the
				table view. Once the file has been saved, open it up in
				<productname>LibreOffice</productname> and modify the data to suit your needs.
				For example, in case of a labelling with &c13;, with an efficiency of almost
				98&nbsp;%, lets's change the &c12; abundance (probability) to&nbsp;0.02 and the
				&c13; abundance to&nbsp;0.98.

			</para>

			<para>

				Now that the modifications were performed, save the file under the same
				format and load it in the <guilabel>IsoSpec standard user
				data</guilabel> tab of the dialog window by clicking <guilabel>Load
				table from file</guilabel>.  The table view now shows the new carbon
				abundance values. From now on, use this tab exactly like you did for the
				standard isospec <emphasis>static</emphasis> data work described above.

			</para>

			<para>

				Here also, it is possible to select determinate rows and then save
				them to a file for later use.

			</para>

		</sect3>

		<sect3 xml:id="sec_user-hand-made-configuration-within-minexpert">

			<title>User hand-made configuration of the element data within mineXpert</title> 

			<para>

        The last way the user might configure the chemical elements to be used
        during an isotopic cluster calculation is based on the fully manual
        description of the elements and of the isotopes. That configuration is
        performed in the <guilabel>Manual configuration</guilabel> tab of the
        dialog window.  This method is slightly more involved than the previous
        one but provides also for a much greater flexibility: it allows one to
        create <quote>new chemical elements</quote> that might be required in
        specific labelling experiments.  The manual configuration is carried
        over in the <guilabel>Manual configuration</guilabel> tab of the dialog,
        as shown in <xref
        linkend="fig_minexpert-manual-configuration-tab-isospec-dlg"/>.</para>


			<figure xml:id="fig_minexpert-manual-configuration-tab-isospec-dlg">

				<title>Hand-made user configuration of the chemical elements and formula</title>

				<mediaobject>

					<imageobject role="fo">
						<imagedata fileref="print-minexpert-manual-configuration-tab-isospec-dlg.png"/>
					</imageobject>
					<imageobject role="html">
						<imagedata fileref="web-minexpert-manual-configuration-tab-isospec-dlg.png"/>
					</imageobject>

					<caption><para>

              When the dialog is created, the tab is empty.  To start creating
              element definitions, click <guibutton>Add element</guibutton>.

						</para>
					</caption>

				</mediaobject>

			</figure>

			<para>

        Upon creation of the dialog window, the <guilabel>Manual
        configuration</guilabel> tab is empty, with only two rows of buttons at
        the bottom of the tab. To start configuring chemical elements, click
        <guibutton>Add element</guibutton> to create an <quote>element group
        box</quote> that contains a number of widgets organized in two
        rows:</para>

		<itemizedlist>

			<listitem>

				<para>

					Top row, a line edit widget to receive the chemical element symbol,
					<guilabel>C</guilabel> in the example; </para>

			</listitem>

			<listitem>

				<para>

					A spin box widget in which to set the number of such atoms in the formula
					for which the isotopic cluster is being calculated. In the example, we set
					this value to <guilabel>769</guilabel>;</para>

			</listitem>

			<listitem>

				<para>

					A button with a <quote>minus</quote> image that removes all the
					<quote>element group box</quote> in one go; </para> 

			</listitem>

			<listitem>

				<para>

					The bottom row contains an <quote>isotope frame
					widget</quote> with two spin boxes for the mass of
					the isotope being configured (left) and its
					corresponding abundance (right); </para>

			</listitem>

			<listitem>

				<para>

					In addition to the spin boxes, two buttons, with a <quote>plus</quote> or
					a <quote>minus</quote> figure, allow one to respectively add or remove isotope
					frames.

				</para>


				<note>

					<para>

						It is not possible to remove all the isotope frames from an element group
						box, otherwise that group box would become useless.

					</para>


				</note>

			</listitem>

		</itemizedlist>

		<para>

			Once an isotope frame has been filled-up, a new line might be required. To
			create a new isotope frame widget, click any <quote>plus</quote>-labelled
			button in any of the isotope frames. Once a new frame is created, the spin box
			widgets that it contains are set to <guilabel>0.00000</guilabel>. Fill-in these
			spin boxes with mass and abundance and go on along this path to create as many
			isotopes as required.

		</para>


		<para>

			Once all the isotopes for a given chemical element have been defined, a new
			element might be needed. For this, click <guibutton>Add element</guibutton> and
			start the configuration of the new element as described above.

		</para>


		<para>

			The manual isotopic configuration of the chemical elements required to
			perform an isotopic cluster calculation for a given formula is tedious. The
			user may want to save a given configuration to a file (click <guilabel>Save
			configuration</guilabel>) so that it is easier to recreate automatically all
			the widgets upon loading of that saved configuration (click  <guilabel>Load
			configuration</guilabel>).

		</para>


		<para>

			The final configuration is shown in <xref
			linkend="fig_web-minexpert-completed-manual-configuration-tab-isospec-dlg"/>.
			The experiment that was configured above is a labelling of a glucose
			molecule with <guilabel>Cz</guilabel>, an imaginary chemical element that is
			like carbon but that has a &c14;. The
			glucose molecule (normal formula: C6H12O6) is labelled on one single carbon
			atom with an efficieny of 95&nbsp;%. This means that, when the labelling
			fails (in 5&nbsp;% of the cases) the carbon atom has its isotopes with usual
			probabilities (compounded by the fact that the normal atom is found at that
			position only in 5&nbsp;% of the cases). The isotopic abundances for the Cz
			element are thus:	</para> 

		<itemizedlist>

			<listitem>
				<para>
					For &c12;: 0.05 * normal &c12; abundance;
				</para>
			</listitem>

			<listitem>
				<para>
					For &c13;: 0.05 * normal &c13; abundance;
				</para>
			</listitem>

			<listitem>
				<para>
					For &c14;: 0.95;
				</para>
			</listitem>

		</itemizedlist>


		<figure xml:id="fig_web-minexpert-completed-manual-configuration-tab-isospec-dlg">

			<title>Typical manual configuration of the isotopic characteristics of the chemical elements</title>

			<mediaobject>

				<imageobject role="fo">
					<imagedata fileref="print-minexpert-completed-manual-configuration-tab-isospec-dlg.png"/>
				</imageobject>
				<imageobject role="html">
					<imagedata fileref="web-minexpert-completed-manual-configuration-tab-isospec-dlg.png"/>
				</imageobject>

				<caption><para>

						The user has configured a labelling experiment where the glucose
						molecule is labelled at a single carbon position with a
						&c14; atom (the efficiency of the labelling is
						95&nbsp;%).

					</para>
				</caption>

			</mediaobject>

		</figure>


		<note>

			<para>

        Note that the <quote>normal</quote> carbon count is
        <guilabel>5</guilabel> (and not 6), that the hydrogen count is 13 (and
        not 12, because the glucose is protonated) and the labelled carbon is
        present only once.

			</para>


		</note>

	</sect3>

	<sect3 xml:id="sec_isospec-results-are-not-shaped-mass-peaks">

		<title>The IsoSpec results are not shaped mass peaks</title>

		<para>

			Once the configurations have been terminated, the calculations can finally
			be performed by the <productname>IsoSpec</productname> library. In the manual
			configuration setting, the formula is automatically handled since each
			chemical element that is defined goes along with the count of the correponding
			atoms. In the case of the standard IsoSpec configuration (either modified or
			not), the user has to enter the chemical formula of the analyte in the
			<guilabel>Formula</guilabel> line edit widget.

		</para>


		<para>

			Click <guilabel>Run</guilabel>. If the configuration was correct and
			<productname>IsoSpec</productname> could run the calculation properly, then
			the dialog window switches to the <guilabel>IsoSpec results</guilabel> tab
			(<xref
			linkend="fig_web-minexpert-radiolabelling-isotopic-cluster-results-tab-dlg"/>).
			That tab contains a text edit widget in which the results are displayed.
		</para>

		<para xml:id="para_account-for-charge-in-isospec-results"> Note that the &mz; values calculated by
		<productname>IsoSpec</productname> are <quote>corrected</quote> for the
		charge level that was specified in the left panel of the dialog window prior to their display in the results tab
		(<xref
		linkend="fig_minexpert-isospec-cluster-calculation-dlg"/>).

	</para>



	<figure xml:id="fig_web-minexpert-radiolabelling-isotopic-cluster-results-tab-dlg">

		<title>Results from the isotopic cluster calculation</title>

		<mediaobject>

			<imageobject role="fo">
				<imagedata fileref="print-minexpert-radiolabelling-isotopic-cluster-results-tab-dlg.png"/>
			</imageobject>
			<imageobject role="html">
				<imagedata fileref="web-minexpert-radiolabelling-isotopic-cluster-results-tab-dlg.png"/>
			</imageobject>

			<caption><para>

          The <productname>IsoSpec</productname> library computes the
          probability of the various combinations of all the isotopes that make
          the chemical formula submitted to it. The results are in the form of
          peak centroid values along with corresponding probabilities. The sum
          of the probabilities corresponds to the <guilabel>Max. cumulative
          probability</guilabel> value that was set by the user.

				</para>
			</caption>

		</mediaobject>

	</figure>


	<para>

		The results that are produced by <productname>IsoSpec</productname>
		represent the peak centroids of the isotopic cluster. The results are thus a
		set of &mzi; pairs that have not the characteristic shape (the profile) that
		is found in mass spectra. &mineXp2; features the ability to give a shape to the
		centroid peaks. For that, click <guilabel>To peak shaper</guilabel> to open
		the <guilabel>Peak Shaper</guilabel> dialog window preloaded with the
		<productname>IsoSpec</productname>-generated peak centroids. The workings of
		this peak shaping feature is described in <xref
		linkend="sec_shaping-mass-peak-centroids"/>.

	</para>


</sect3>


</sect2>

</sect1>


<sect1 xml:id="sec_shaping-mass-peak-centroids">

	<title>Shaping mass peak centroids into well-profiled peaks</title>

	<para>

		The shape of mass peaks is typically Gaussian or Lorentzian (or a mix
		thereof).  There are some data simulation or analysis processes that lead to
		having mass peaks characterized by a single centroid m/z value and a
		corresponding intensity. Plotted to a graph, a centroid mass peak yields a
		bar. In order to convert mass peak centroids into something that resembles a
		real <quote>profile</quote> mass peaks, a mathematical formula can be
		applied, with some parameters to configure the shapes generated. &mineXp2;
		includes that feature, accessible <emphasis>via</emphasis> the


		<menuchoice>
			<guimenu>Utilities</guimenu>
			<guimenuitem>Mass peak shape	calculations</guimenuitem>
			</menuchoice> menu item.

			The window that opens up is shown in  <xref
			linkend="fig_minexpert-peak-shaper-input-tab-dlg"/></para>


	<figure xml:id="fig_minexpert-peak-shaper-input-tab-dlg">

		<title>Setting-up of the centroid mass peak shaping process</title>

		<mediaobject>

			<imageobject role="fo">
				<imagedata fileref="print-minexpert-peak-shaper-input-tab-dlg.png" scale="80"/>
			</imageobject>

			<imageobject role="html">
				<imagedata fileref="web-minexpert-peak-shaper-input-tab-dlg.png"/>
			</imageobject>

			<caption><para>

					This dialog window allows one to configure the shaping of mass
					centroid peaks. Setting a spectrum name in the <guilabel>Mass spectrum
					name</guilabel> line edit widget will help recognize the result mass
					spectrum once displayed in the <guilabel>Mass spectra</guilabel>
					window (see below).

				</para>
			</caption>

		</mediaobject>

	</figure>

	<para>

		The mass centroid peaks are listed in the <guilabel>Data centroid points
		(m/z,i)</guilabel> text edit widget. These values are pasted there by the
		user or copied automatically from the isotopic cluster calculation dialog
		window (see <xref
		linkend="sec_isospec-results-are-not-shaped-mass-peaks"/>).  The width of
		the <quote>profile</quote> mass peak is determined either by setting the
		resolution of the instrument (in the example, that is set to 45000) or by
		setting the width of the peak at half maximum of its height (FWHM).

	</para>

	<para>

		The spectrum that is generated can be of a Gaussian or a Lorentzian shape.
		That parameter is configured by selecting the corresponding radio button
		widget. The number of points used to actually craft the shape of the peak is
		configurable. In the example, that parameter is set to 200.

	</para>

	<tip>

		<para>

			When defining the parameters for the peak shaping process, it might be
			useful to have and idea of the <acronym>FWHM</acronym> value at a given
			&mz; value for a given resolution. To compute that
			<acronym>FWHM</acronym>value, double-click-select a single mass peak
			centroid &mz; value from the text edit widget, set the resolving power of
			your instrument and then click <guibutton>To FWHM</guibutton>.  The
			computed <acronym>FWHM</acronym> value will be displayed next to the
			button. The text is selectable so as to copy it to the clipboard and then
			in the <guilabel>FWHM</guilabel> spin box widget.

		</para>


	</tip>

	<para>

		Once the peak shaping parameters have been set, click
		<guibutton>Run</guibutton>. The dialog window shifts tab to
		<guilabel>Results</guilabel>, as shown in <xref
		linkend="fig_minexpert-peak-shaper-results-tab-dlg"/>.

	</para>


	<figure xml:id="fig_minexpert-peak-shaper-results-tab-dlg">

		<title>Shaped peaks mass spectral data</title>

		<mediaobject>

			<imageobject role="fo">
				<imagedata fileref="print-minexpert-peak-shaper-results-tab-dlg.png"/>
			</imageobject>
			<imageobject role="html">
				<imagedata fileref="web-minexpert-peak-shaper-results-tab-dlg.png"/>
			</imageobject>

			<caption> 

				<para>

					The mass spectral data corresponding to the combination of each
					individual <quote>peak-shaped</quote> centroid peak are displayed in
					this tab. Plotting these data will produce a nicely shaped isotopic
					cluster matching what would be seen by recording a mass spectrum on an
					instrument.

				</para> 

			</caption>

		</mediaobject>

	</figure>


	<tip>

		<para>

			To ensure that various isotopic cluster calculation-based mass spectra can
			later be recognized, the user is advised, for the different simulations,
			to insert distinct names in the <guilabel>Mass spectrum name</guilabel>
			line edit widget. This name will be used later when creating the mass
			spectrum if the user asks that the mass spectrum be displayed by clicking
			<guibutton>Display mass spectrum</guibutton>. In the eventuality that the
			mass spectrum name is not changed from a simulation to the other, a
			safeguard process ensures that names are absolutely unambiguous by
			appending to the mass spectrum name the time at which the mass spectrum is
			displayed.

		</para>

	</tip>

	<para>

		It is possible to automatically plot the result mass spectrum by clicking
		<guibutton>Display mass spectrum</guibutton>. The process gets a little
		involved here because the mass spectrum is not created from a file, and
		&mineXp2;'s handling of plots is based on the fact that a MS run data set has
		been created, typically at data file loading time. Two situations may arise:

		<itemizedlist>

			<listitem>

				<para>

					If not a single MS run data set is currently available (that is, no
					single mass spectrometry data file was loaded during the current
					session), the program will inform the user that a shim MS run data set
					was created (<xref
					linkend="fig_minexpert-peak-shaper-shim-data-file-info-dlg"/>). At
					this time, the dialog window listing all the open MS run data sets
					will show up (<xref
					linkend="fig_minexpert-open-ms-run-data-sets-dlg"/>). The information
					message advises to perform the task again, that is, click
					<guibutton>Display mass spectrum</guibutton>.

					<figure xml:id="fig_minexpert-peak-shaper-shim-data-file-info-dlg">

						<title>Creation of a shim MS run data set</title>

						<mediaobject>

							<imageobject role="fo">
								<imagedata fileref="print-minexpert-peak-shaper-shim-data-file-info-dlg.png"/>
							</imageobject>
							<imageobject role="html">
								<imagedata fileref="web-minexpert-peak-shaper-shim-data-file-info-dlg.png"/>
							</imageobject>

							<caption> 

                <para>This information dialog window informs the user that a new
                shim MS run data set was created. Shim MS run data sets need to
                be created when new plots are created for data that were not
                loaded from a disk file. The user is advised to repeat the last
                action. In this case, click <guibutton>Display mass
                spectrum</guibutton> again. The next step is described in the
                text below.</para> 

						</caption>

					</mediaobject>

				</figure>

			</para>

		</listitem>

		<listitem>

			<para>

				If at least one MS run data set was loaded already in the program, then
				an input dialog window shows up letting the user select the MS run data
				set to which the isotopic cluster calculation mass spectrum is to be
				anchored to (<xref
				linkend="fig_minexpert-peak-shaper-msrundataset-selection-dlg"/>). It is
				always possible to ask that a new MS run data set be created
				<foreignphrase>ex nihilo</foreignphrase> for the new mass spectrum by
				selecting <guilabel>Create a new MS run data set</guilabel>. In this
				case, the process goes back to the previous item. This process is useful
				if each mass spectrum must have a distinct color.

			</para>

			<figure xml:id="fig_minexpert-peak-shaper-msrundataset-selection-dlg">

				<title>Selection of a MS run data set</title>

				<mediaobject>

					<imageobject role="fo">
						<imagedata fileref="print-minexpert-peak-shaper-msrundataset-selection-dlg.png"/>
					</imageobject>
					<imageobject role="html">
						<imagedata fileref="web-minexpert-peak-shaper-msrundataset-selection-dlg.png"/>
					</imageobject>

					<caption> 

						<para>If one or more MS run data set(s) preexisted in the program,
						this input dialog shows up (left). Upon clicking the combo box
						widget, the user can select the MS run data set to anchor the new
						mass spectrum to (right).</para> 

				</caption>

			</mediaobject>

		</figure>

	</listitem>

</itemizedlist>

</para>


<para>

	The isotopic cluster calculation-based mass spectrum shows up in the
	<guilabel>Mass spectra</guilabel> window, as shown in <xref
	linkend="fig_minexpert-peak-shaper-results-spectrum"/>.

</para>

<figure xml:id="fig_minexpert-peak-shaper-results-spectrum">

	<title>Spectrum created using the peak shaping feature</title>

	<mediaobject>

		<imageobject role="fo">
			<imagedata fileref="print-minexpert-peak-shaper-results-spectrum.png"/>
		</imageobject>
		<imageobject role="html">
			<imagedata fileref="web-minexpert-peak-shaper-results-spectrum.png"/>
		</imageobject>

		<caption>

			<para>

				The spectrum corresponds to a combination of each individual spectrum obtained
				by shaping each individual mass peak centroid in the input data list.

			</para>

		</caption>

	</mediaobject>

</figure>

<para>

	Note that if the resolution asked is very high, the resulting shaped mass
	peaks might appear a bit <quote>hairy</quote>. By tweaking the <guilabel>Bin
	size</guilabel> value, the binning of the spectra might improve the
	situation. Otherwise, using the plot widget main menu to apply a
	Savitzky-Golay filter, described at <xref
	linkend="para_very-high-resolution-hairy-peaks-savtizky-golay-filter"/>,
	will certainly improve things.

</para>


<tip>

	<para>

		If the peak centroids were not <quote>corrected</quote> for their charge
		in the previous generation step (as in the case of the isotopic cluster
		calculation, it is still time to apply this <quote>correction</quote> by
		setting the charge in the <guilabel>Charge</guilabel> spin box widget.
		If the charge was already accounted for, as described in <xref
		linkend="para_account-for-charge-in-isospec-results"/>, then leave the
		charge to <guilabel>1</guilabel> and the results will be correct.

	</para>


</tip> 


</sect1>

</chapter>



